package engine.renderer.shadertools;

import java.util.HashMap;
import java.util.HashSet;

import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;

//TODO: need to ignore comments in GLSL
//otherwise, this will match stuff in block comments

//CUSTOM PREPROCESSOR SPECIFICATION:
//-------------------------------------------------------------------------
//[w] = spaces and tabs
//handle lines which match: [w]'#'[w][PREPROCESSOR_TOKEN][w][source_identifier][w]
//that line will be replaced by the function definition in the specified source file
//the source_identifier must be alphanum, starting with alpha

//all lines are simply included, EXCEPT:
//strip custom preprocessor line if it references an already-included source_identifier
//strip any other line IFF:
//  1) it matches the "declaration_statement" pattern indicated below
//  2) it is before any '{' has been encountered and is on its own line
//  3) IDENTIFIER is in already-included variables
//-------------------------------------------------------------------------

//FROM GLSL ES SPEC [ignored things indicated by **]
//-------------------------------------------------------------------------
//declaration_statement: declaration: init_declarator_list SEMICOLON
//init_declarator_list: single_declaration **ignoring other things with "COMMA IDENTIFIER"**
//
//single_declaration:  
//<fully_specified_type> **has no IDENTIFIER, so irrelevant**
//fully_specified_type IDENTIFIER
//fully_specified_type IDENTIFIER LEFT_BRACKET constant_expression RIGHT_BRACKET
//fully_specified_type IDENTIFIER EQUAL initializer
//INVARIANT IDENTIFIER
//
//fully_specified_type: type_specifier, type_qualifier type_specifier
//	type_qualifier: CONST, ATTRIBUTE, VARYING, INVARIANT VARYING, UNIFORM
//	type_specifier: type_specifier_no_prec, precision_qualifier type_specifier_no_prec
//		precision_qualifier: HIGH_PRECISION, MEDIUM_PRECISION, LOW_PRECISION
//		type_specifier_no_prec: VOID FLOAT INT BOOL VEC2 VEC3 VEC4 BVEC2 BVEC3 BVEC4 
//			IVEC2 IVEC3 IVEC4 MAT2 MAT3 MAT4 SAMPLER2D SAMPLERCUBE **struct_specifier, TYPE_NAME**
//-------------------------------------------------------------------------

//TO RECOGNIZE IDENTIFIERS:
//-------------------------------------------------------------------------
//(type_qualifier) (precision_qualifier) type_specifier_no_prec IDENTIFIER SEMICOLON
//(type_qualifier) (precision_qualifier) type_specifier_no_prec IDENTIFIER LEFT_BRACKET constant_expression RIGHT_BRACKET SEMICOLON
//(type_qualifier) (precision_qualifier) type_specifier_no_prec IDENTIFIER EQUAL initializer SEMICOLON
//-------------------------------------------------------------------------


public abstract class GLSLESPreprocessor{
	
	protected static final String PREPROCESSOR_TOKEN = "shared";
	
	protected static final String GLSL_TYPE_QUALIFIER = "const|attribute|varying|invariant varying|uniform";
	protected static final String GLSL_PRECISION_QUALIFIER = "high_precision|medium_precision|low_precision";
	protected static final String GLSL_TYPE_SPECIFIER_NO_PREC = "void|float|int|bool|vec2|vec3|vec4|bvec2|bvec3|bvec4|ivec2|ivec3|ivec4|mat2|mat3|mat4|sampler2d|samplercube";
	
	protected static final String REGEXP_ALPHANUM = "[A-Za-z_]+[A-Za-z0-9_]*";
	
	protected static final String REGEXP_SOURCE_IDENTIFIER = "[ \t]*#[ \t]*" + PREPROCESSOR_TOKEN + "[ \t]+(" + REGEXP_ALPHANUM + ")";
	protected static final RegExp PATTERN_SOURCE_IDENTIFIER = RegExp.compile(REGEXP_SOURCE_IDENTIFIER, "i");
	
	protected static final String REGEXP_GLSL_IDENTIFIER = "[ \t]*(?:(?:" + GLSL_TYPE_QUALIFIER + ")[ \t]+)?(?:(?:" + GLSL_PRECISION_QUALIFIER + ")[ \t]+)?(?:" + GLSL_TYPE_SPECIFIER_NO_PREC + ")[ \t]+(" + REGEXP_ALPHANUM + ")[^(]*;[ \t]*";
	protected static final RegExp PATTERN_GLSL_IDENTIFIER = RegExp.compile(REGEXP_GLSL_IDENTIFIER, "i");
	
	
	public static String process(String source, HashMap<String,String> sourceIdentifiers){
		
		return process(source, sourceIdentifiers,
			new HashSet<String>(), new HashSet<String>());
	}
	
	protected static String process(String source, HashMap<String,String> sourceIdentifiers,
		HashSet<String> alreadyIncludedSourceIdentifiers, HashSet<String> alreadyIncludedGLSLIdentifiers){
		
		String code = "";
		boolean encounteredLeftBrace = false;
		
		String[] lines = source.split("\\r?\\n");
		
		for(String line : lines){
			//update encounteredLeftBrace
			encounteredLeftBrace = encounteredLeftBrace || line.contains("{");
			//if a '{' has been encountered, simply include the line; otherwise, process
			code += encounteredLeftBrace ? (line + "\n") : 
				processLine(line,sourceIdentifiers,alreadyIncludedSourceIdentifiers,alreadyIncludedGLSLIdentifiers);
		}
		
		return code;
	} 
	
	protected static String processLine(String line, HashMap<String,String> sourceIdentifiers,
		HashSet<String> alreadyIncludedSourceIdentifiers, HashSet<String> alreadyIncludedGLSLIdentifiers){
		
		//perform regexp checks
		MatchResult m;
		
		//check for preprocessor source identifier
		m = PATTERN_SOURCE_IDENTIFIER.exec(line);
		if(m != null){
			//get match
			String identifier = m.getGroup(1);
			
			//if already in list of processed source identifiers, return blank string
			if(alreadyIncludedSourceIdentifiers.contains(identifier)){ return ""; }
			//else, add to list of processed source identifiers, and recurse
			alreadyIncludedSourceIdentifiers.add(identifier);
			return process(sourceIdentifiers.get(identifier), sourceIdentifiers, alreadyIncludedSourceIdentifiers, alreadyIncludedGLSLIdentifiers);
		}
		
		//check for GLSL identifier
		m = PATTERN_GLSL_IDENTIFIER.exec(line);
		if(m != null){
			//get match
			String identifier = m.getGroup(1);
			
			//if already in list of GLSL identifiers, return blank string
			if(alreadyIncludedGLSLIdentifiers.contains(identifier)){ return ""; }
			//else, add to list of GLSL identifiers
			alreadyIncludedGLSLIdentifiers.add(identifier);
		}
		
		//return line + newline if not a source identifier, or not an already-processed GLSL identifier
		return line + "\n";
	}
}
