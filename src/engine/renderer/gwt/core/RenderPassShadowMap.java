package engine.renderer.gwt.core;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;

import engine.renderer.DrawableIBO;
import engine.renderer.gwt.object.WGLBuffer;
import engine.renderer.gwt.object.WGLTexture;
import engine.renderer.gwt.shader.ShaderProgram;
import engine.renderer.gwt.shader.lightpass.ShaderShadowMap;
import engine.scene.SceneObject;
import engine.scene.SceneTraversal;
import engine.terrain.Terrain;
import geometry.math.Camera;
import geometry.math.Vector2d;
import geometry.spacepartition.Box;

public class RenderPassShadowMap 
	extends SceneTraversal<WebGLRenderingContext,WGLBuffer,WGLTexture>{
	
	private static final boolean CHECK_VALIDATION = false; //[TODO] for debugging

	private ShaderShadowMap shaderMask;
	private ShaderShadowMap shaderNoMask;
	//private ShaderTerrainShadowMap shaderTerrain;
	
	//transient data for a single pass
	protected WebGLRenderingContext gl;
	protected Camera camera;
	
	//things that can change; store last so not flipped every object
	protected WGLTexture lastActiveTexColor;
	protected ShaderProgram lastActiveObjectShader;
	
	
	public RenderPassShadowMap(WebGLRenderingContext gl){
		shaderMask = new ShaderShadowMap(gl, true);
		shaderNoMask = new ShaderShadowMap(gl, false);
		//shaderTerrain = new ShaderTerrainShadowMap(gl);
	}
	
	public void initializePass(WebGLRenderingContext gl, Camera camera){
		this.gl = gl;
		this.camera = camera;
		
		//no active texture
		lastActiveTexColor = null;
		lastActiveObjectShader = null;

		setupShadowMapPass();
	}
	
	private void setupShadowMapPass(){
		
		gl.enable(WebGLRenderingContext.DEPTH_TEST);
		gl.depthFunc(WebGLRenderingContext.LESS);
		
		gl.depthMask(true); 
		gl.colorMask(false, false, false, false);
		
		gl.disable(WebGLRenderingContext.BLEND);
		
		//gl.glEnable(WebGLRenderingContext.CULL_FACE);
		//gl.glCullFace(WebGLRenderingContext.BACK);
	}
	
	@Override
	public void actionObject(SceneObject<WebGLRenderingContext,WGLBuffer,WGLTexture> sceneObject){
		
		if(sceneObject.getMaterial().hasAlphaMask()){
			bindShaderMask();
			bindTexColor(sceneObject.getMaterial().getTexColor());
			
			shaderMask.setObjectTransformation(gl, sceneObject);
			shaderMask.setVertexAttributeBuffers(gl, sceneObject.getDrawable());
		}else{
			bindShaderNoMask();
			
			shaderNoMask.setObjectTransformation(gl, sceneObject);
			shaderNoMask.setVertexAttributeBuffers(gl, sceneObject.getDrawable());
		}

		if(CHECK_VALIDATION){ lastActiveObjectShader.checkValidateStatus(gl); } //[TODO]
		gl.drawArrays(WebGLRenderingContext.TRIANGLES, 0, 3*sceneObject.getDrawable().getTriCount());
	}
	
	@Override
	public void actionTerrainBegin(Terrain<WebGLRenderingContext,WGLBuffer,WGLTexture> terrain){
		
		//[TODO]
		/*
		//check if an object shader is bound 
		if(lastActiveObjectShader != null){ 
			lastActiveObjectShader.unuseShader(gl); 
			lastActiveObjectShader = null;
		}
		
		//bind and prepare generic terrain shader uniforms
		shaderTerrain.useShader(gl);
		shaderTerrain.setViewMatrices(gl, camera);
		
		shaderTerrain.setVertexAttributeBuffers(gl, terrain.getPositionBuffer());
		shaderTerrain.setTerrainMetrics(gl, terrain.getMetrics());
		
		//bind necessary terrain textures
		terrain.getDataHeightAndAux().bind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);
		*/
	}
	
	@Override
	public void actionTerrainTile(DrawableIBO<WebGLRenderingContext,WGLBuffer> tileIBO, Box<Vector2d> tileLerp) {
		
		//[TODO]
		/*
		shaderTerrain.setTileLerp(gl, tileLerp.getMin(), tileLerp.getMax());
		
		tileIBO.getIndexBuffer().bindAsElementArray(gl);
		
		if(CHECK_VALIDATION){ shaderTerrain.checkValidateStatus(gl); } //[TODO]
		gl.glDrawElements(WebGLRenderingContext.TRIANGLES, 3*tileIBO.getTriCount(), WebGLRenderingContext.UNSIGNED_SHORT, 0);
		*/
	}

	@Override
	public void actionTerrainEnd(Terrain<WebGLRenderingContext,WGLBuffer,WGLTexture> terrain) {
		
		//[TODO]
		/*
		shaderTerrain.unuseShader(gl);
		
		terrain.getDataHeightAndAux().unbind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);
		*/
	}
	
	protected boolean bindTexColor(WGLTexture texture){
		//bind texture if it's not already, first unbinding previous texture if there is one
		//returns false if it did not bind anything
		
		if(texture == lastActiveTexColor){ return false; }
		if(lastActiveTexColor != null){ lastActiveTexColor.unbind(gl, ShaderShadowMap.COLOR_TEXTURE_UNIT); }
		
		texture.bind(gl, ShaderShadowMap.COLOR_TEXTURE_UNIT);
		lastActiveTexColor = texture;
		return true;
	}
	
	protected boolean bindShaderMask(){
		
		//check if already bound, or if another shader is bound and needs to be unbound 
		if(lastActiveObjectShader == shaderMask){ return false; }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		shaderMask.useShader(gl);
		shaderMask.setViewMatrices(gl, camera);
		
		lastActiveObjectShader = shaderMask;
		return true;
	}
	
	protected boolean bindShaderNoMask(){
		
		//check if already bound, or if another shader is bound and needs to be unbound 
		if(lastActiveObjectShader == shaderNoMask){ return false; }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		shaderNoMask.useShader(gl);
		shaderNoMask.setViewMatrices(gl, camera);
		
		lastActiveObjectShader = shaderNoMask;
		return true;
	}
	
	public void finishPass(){
		
		//unbind texture and shader, if there is one bound
		if(lastActiveTexColor != null){ lastActiveTexColor.unbind(gl, ShaderShadowMap.COLOR_TEXTURE_UNIT); }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		//unset transient data
		this.gl = null;
		camera = null;
	}
}
