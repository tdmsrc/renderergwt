package engine.renderer.gwt.core;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;

import engine.renderer.DrawableIBO;
import engine.renderer.Material;
import engine.renderer.gwt.object.WGLBuffer;
import engine.renderer.gwt.object.WGLTexture;
import engine.renderer.gwt.shader.ShaderProgram;
import engine.renderer.gwt.shader.lightpass.ShaderAmbientAndEmissive;
import engine.scene.SceneObject;
import engine.scene.SceneTraversal;
import engine.terrain.Terrain;
import geometry.math.Camera;
import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.spacepartition.Box;


//very similar to RenderPassLightGeneric, but with ambient light properties
//instead of a light, and with additional emissive light properties

public class RenderPassAmbientAndEmissive extends SceneTraversal<WebGLRenderingContext,WGLBuffer,WGLTexture>{

	private ShaderAmbientAndEmissive shaderBump;
	private ShaderAmbientAndEmissive shaderNoBump;
	//private ShaderTerrainAmbientAndEmissive shaderTerrain;
	
	//transient data for a single pass
	protected WebGLRenderingContext gl;
	protected Camera camera;
	protected float ambientCoefficient;
	protected Vector3d ambientColor;
	protected float fogDensity;
	
	//things that can change; store last so not flipped every object
	protected WGLTexture lastActiveTexColor, lastActiveTexBump;
	protected ShaderProgram lastActiveObjectShader;
	
	
	public RenderPassAmbientAndEmissive(WebGLRenderingContext gl){
		shaderBump = new ShaderAmbientAndEmissive(gl, true);
		shaderNoBump = new ShaderAmbientAndEmissive(gl, false);
		//shaderTerrain = new ShaderTerrainAmbientAndEmissive(gl);
	}
	
	public void initializePass(WebGLRenderingContext gl, Camera camera, Vector3d ambientColor, float ambientCoefficient, float fogDensity){
		this.gl = gl;
		this.camera = camera;
		this.ambientCoefficient = ambientCoefficient;
		this.ambientColor = ambientColor;
		this.fogDensity = fogDensity;
		
		//no active texture
		lastActiveObjectShader = null;
		lastActiveTexColor = null;
		lastActiveTexBump = null;
		
		setupAmbientAndEmissivePass();
	}
	
	private void setupAmbientAndEmissivePass(){
		
		gl.enable(WebGLRenderingContext.DEPTH_TEST);
		gl.depthFunc(WebGLRenderingContext.LEQUAL);
		
		gl.depthMask(false); 
		gl.colorMask(true, true, true, false);
		
		gl.enable(WebGLRenderingContext.BLEND);
		gl.blendFunc(WebGLRenderingContext.ONE, WebGLRenderingContext.ONE);
		
		//gl.enable(WebGLRenderingContext.CULL_FACE);
		//gl.cullFace(WebGLRenderingContext.BACK);
	}

	@Override
	public void actionObject(SceneObject<WebGLRenderingContext,WGLBuffer,WGLTexture> sceneObject){
		
		Material<WGLTexture> mtl = sceneObject.getMaterial();
		
		if(mtl.hasTexBump() && sceneObject.getDrawable().hasTangentData()){
			bindshaderBump();
			bindTexColor(mtl.getTexColor());
			
			bindTexBump(mtl.getTexBump());
			shaderBump.setReliefMappingHeight(gl, mtl.getReliefMappingHeight());
			
			shaderBump.setEmissive(gl, sceneObject.getDrawOptions());
			shaderBump.setObjectTransformation(gl, sceneObject);
			shaderBump.setVertexAttributeBuffers(gl, sceneObject.getDrawable());
		}else{
			bindshaderNoBump();
			bindTexColor(mtl.getTexColor());
			
			shaderNoBump.setEmissive(gl, sceneObject.getDrawOptions());
			shaderNoBump.setObjectTransformation(gl, sceneObject);
			shaderNoBump.setVertexAttributeBuffers(gl, sceneObject.getDrawable());
		}

		gl.drawArrays(WebGLRenderingContext.TRIANGLES, 0, 3*sceneObject.getDrawable().getTriCount());
	}
	
	@Override
	public void actionTerrainBegin(Terrain<WebGLRenderingContext,WGLBuffer,WGLTexture> terrain){
		
		//[TODO]
		/*
		//check if an object shader is bound 
		if(lastActiveObjectShader != null){ 
			lastActiveObjectShader.unuseShader(gl); 
			lastActiveObjectShader = null;
		}
		
		//bind and prepare generic terrain shader uniforms
		shaderTerrain.useShader(gl);
		shaderTerrain.setViewMatrices(gl, camera);
		
		shaderTerrain.setVertexAttributeBuffers(gl, terrain.getPositionBuffer());
		shaderTerrain.setTerrainMetrics(gl, terrain.getMetrics());
		
		//pass-specific shader uniforms
		shaderTerrain.setFog(gl, fogDensity);
		shaderTerrain.setAmbient(gl, ambientColor, ambientCoefficient);
		shaderTerrain.setEmissive(gl, terrain.getDrawOptions());
		
		//bind necessary terrain textures
		terrain.getDataHeightAndAux().bind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);
		
		terrain.getDataNormalAndBlend().bind(gl, ShaderTerrainAmbientAndEmissive.NORMAL_AND_BLEND_TEXTURE_UNIT);
		terrain.getMaterial().getMaterial1().getTexColor().bind(gl, ShaderTerrainAmbientAndEmissive.COLOR1_TEXTURE_UNIT);
		terrain.getMaterial().getMaterial2().getTexColor().bind(gl, ShaderTerrainAmbientAndEmissive.COLOR2_TEXTURE_UNIT);
		*/
	}
	
	@Override
	public void actionTerrainTile(DrawableIBO<WebGLRenderingContext,WGLBuffer> tileIBO, Box<Vector2d> tileLerp) {
		
		//[TODO]
		/*
		shaderTerrain.setTileLerp(gl, tileLerp.getMin(), tileLerp.getMax());
		
		tileIBO.getIndexBuffer().bindAsElementArray(gl);
		gl.drawElements(WebGLRenderingContext.TRIANGLES, 3*tileIBO.getTriCount(), WebGLRenderingContext.UNSIGNED_SHORT, 0);
		*/
	}

	@Override
	public void actionTerrainEnd(Terrain<WebGLRenderingContext,WGLBuffer,WGLTexture> terrain) {
		
		//[TODO]
		/*
		shaderTerrain.unuseShader(gl);
		
		terrain.getDataHeightAndAux().unbind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);
		
		terrain.getDataNormalAndBlend().unbind(gl, ShaderTerrainAmbientAndEmissive.NORMAL_AND_BLEND_TEXTURE_UNIT);
		terrain.getMaterial().getMaterial1().getTexColor().unbind(gl, ShaderTerrainAmbientAndEmissive.COLOR1_TEXTURE_UNIT);
		terrain.getMaterial().getMaterial2().getTexColor().unbind(gl, ShaderTerrainAmbientAndEmissive.COLOR2_TEXTURE_UNIT);
		*/
	}
	
	protected boolean bindTexColor(WGLTexture texture){
		//bind texture if it's not already, first unbinding previous texture if there is one
		//returns false if it did not bind anything
		
		if(texture == lastActiveTexColor){ return false; }
		if(lastActiveTexColor != null){ lastActiveTexColor.unbind(gl, ShaderAmbientAndEmissive.COLOR_TEXTURE_UNIT); }
		
		texture.bind(gl, ShaderAmbientAndEmissive.COLOR_TEXTURE_UNIT);
		lastActiveTexColor = texture;
		return true;
	}
	
	protected boolean bindTexBump(WGLTexture texture){
		//bind texture if it's not already, first unbinding previous texture if there is one
		//returns false if it did not bind anything
		
		if(texture == lastActiveTexBump){ return false; }
		if(lastActiveTexBump != null){ lastActiveTexBump.unbind(gl, ShaderAmbientAndEmissive.BUMP_TEXTURE_UNIT); }
		
		texture.bind(gl, ShaderAmbientAndEmissive.BUMP_TEXTURE_UNIT);
		lastActiveTexColor = texture;
		return true;
	}
	
	protected boolean bindshaderBump(){
		
		//check if already bound, or if another shader is bound and needs to be unbound 
		if(lastActiveObjectShader == shaderBump){ return false; }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		shaderBump.useShader(gl);
		shaderBump.setViewMatrices(gl, camera);
		
		shaderBump.setFog(gl, fogDensity);
		shaderBump.setAmbient(gl, ambientColor, ambientCoefficient);
		
		lastActiveObjectShader = shaderBump;
		return true;
	}
	
	protected boolean bindshaderNoBump(){
		
		//check if already bound, or if another shader is bound and needs to be unbound 
		if(lastActiveObjectShader == shaderNoBump){ return false; }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		shaderNoBump.useShader(gl);
		shaderNoBump.setViewMatrices(gl, camera);
		
		shaderNoBump.setFog(gl, fogDensity);
		shaderNoBump.setAmbient(gl, ambientColor, ambientCoefficient);
		
		lastActiveObjectShader = shaderNoBump;
		return true;
	}
	
	public void finishPass(){
		
		//unbind texture and shader, if there is one bound
		if(lastActiveTexColor != null){ lastActiveTexColor.unbind(gl, ShaderAmbientAndEmissive.COLOR_TEXTURE_UNIT); }
		if(lastActiveTexBump != null){ lastActiveTexBump.unbind(gl, ShaderAmbientAndEmissive.BUMP_TEXTURE_UNIT); }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		//unset transient data
		this.gl = null;
		camera = null;
		ambientColor = null;
	}
}
