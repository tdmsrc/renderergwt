package engine.renderer.gwt.core;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;

import engine.renderer.gwt.object.WGLBuffer;
import engine.renderer.gwt.object.WGLTexture;
import engine.renderer.gwt.shader.lightpass.ShaderLightPassDirectional;
import engine.scene.Light;
import engine.terrain.Terrain;
import geometry.math.Camera;


public class RenderPassLightDirectional extends RenderPassLightGeneric
	<ShaderLightPassDirectional>{//, ShaderTerrainLightPassDirectional>{


	public RenderPassLightDirectional(WebGLRenderingContext gl){
		super(
			new ShaderLightPassDirectional(gl, false), 
			new ShaderLightPassDirectional(gl, true));//,
			//new ShaderTerrainLightPassDirectional(gl));
	}
	
	@Override
	public void initializePass(WebGLRenderingContext gl, Light light, Camera camera, float fogDensity){
		super.initializePass(gl, light, camera, fogDensity);
		
	}
	
	@Override
	public void actionTerrainBegin(Terrain<WebGLRenderingContext,WGLBuffer,WGLTexture> terrain){
		super.actionTerrainBegin(terrain);
		
		//[TODO]
		//shaderTerrain.setAmbientToShadowMapTransformation(gl, light);
	}
	
	@Override
	protected boolean bindShader(){
		if(!super.bindShader()){ return false; }
		
		shader.setAmbientToShadowMapTransformation(gl, light);
		return true;
	}
	
	@Override
	protected boolean bindShaderBump(){
		if(!super.bindShaderBump()){ return false; }
		
		shaderBump.setAmbientToShadowMapTransformation(gl, light);
		return true;
	}
}
