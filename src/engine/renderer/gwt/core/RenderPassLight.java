package engine.renderer.gwt.core;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;

import engine.renderer.gwt.shader.lightpass.ShaderLightPass;


public class RenderPassLight extends RenderPassLightGeneric
	<ShaderLightPass>{//, ShaderTerrainLightPass>{

	public RenderPassLight(WebGLRenderingContext gl){
		super(
			new ShaderLightPass(gl, false), 
			new ShaderLightPass(gl, true));//,
			//new ShaderTerrainLightPass(gl));
	}
}
