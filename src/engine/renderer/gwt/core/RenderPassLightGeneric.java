package engine.renderer.gwt.core;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;

import engine.renderer.DrawableIBO;
import engine.renderer.Material;
import engine.renderer.gwt.object.WGLBuffer;
import engine.renderer.gwt.object.WGLTexture;
import engine.renderer.gwt.shader.ShaderProgram;
import engine.renderer.gwt.shader.lightpass.ShaderLightPass;
import engine.scene.Light;
import engine.scene.SceneObject;
import engine.scene.SceneTraversal;
import engine.terrain.Terrain;
import geometry.math.Camera;
import geometry.math.Vector2d;
import geometry.spacepartition.Box;


public class RenderPassLightGeneric
	<ShaderObject extends ShaderLightPass>//,
	// ShaderTerrain extends ShaderTerrainLightPass>
	extends SceneTraversal<WebGLRenderingContext,WGLBuffer,WGLTexture>{
	
	private static final boolean CHECK_VALIDATION = false; //[TODO] for debugging
	
	protected ShaderObject shader;
	protected ShaderObject shaderBump;
	//protected ShaderTerrain shaderTerrain;
	
	//transient data for a single pass
	protected WebGLRenderingContext gl;
	protected Light light;
	protected Camera camera;
	protected float fogDensity;
	
	//things that can change; store last so not flipped every object
	protected WGLTexture lastActiveTexColor, lastActiveTexBump;
	protected ShaderProgram lastActiveObjectShader;
	
	
	public RenderPassLightGeneric(ShaderObject shader, ShaderObject shaderBump){//, ShaderTerrain shaderTerrain){
		
		this.shader = shader;
		this.shaderBump = shaderBump;
		//this.shaderTerrain = shaderTerrain;
	}
	
	public void initializePass(WebGLRenderingContext gl, Light light, Camera camera, float fogDensity){
		this.gl = gl;
		this.light = light;
		this.camera = camera;
		this.fogDensity = fogDensity;
		
		//no active shader or texture
		lastActiveObjectShader = null;
		lastActiveTexColor = null;
		lastActiveTexBump = null;
		
		setupLightPass();
	}
	
	private void setupLightPass(){
		
		gl.enable(WebGLRenderingContext.DEPTH_TEST);
		gl.depthFunc(WebGLRenderingContext.LEQUAL);
		
		gl.depthMask(false); 
		gl.colorMask(true, true, true, false);
		
		gl.enable(WebGLRenderingContext.BLEND);
		gl.blendFunc(WebGLRenderingContext.ONE, WebGLRenderingContext.ONE);
		
		//gl.enable(WebGLRenderingContext.CULL_FACE);
		//gl.cullFace(WebGLRenderingContext.BACK);
	}
	
	@Override
	public void actionObject(SceneObject<WebGLRenderingContext,WGLBuffer,WGLTexture> sceneObject){

		Material<WGLTexture> mtl = sceneObject.getMaterial();
		
		//bind appropriate shader and texture, if necessary, then draw
		if(mtl.hasTexBump() && sceneObject.getDrawable().hasTangentData()){
			bindShaderBump();
			bindTexColor(mtl.getTexColor());
			
			bindTexBump(mtl.getTexBump());
			shaderBump.setReliefMappingHeight(gl, mtl.getReliefMappingHeight());
			
			shaderBump.setMaterialLightingCoefficients(gl, mtl);
			shaderBump.setObjectTransformation(gl, sceneObject);
			shaderBump.setVertexAttributeBuffers(gl, sceneObject.getDrawable());
		}else{
			bindShader();
			bindTexColor(mtl.getTexColor());
			
			shader.setMaterialLightingCoefficients(gl, mtl);
			shader.setObjectTransformation(gl, sceneObject);
			shader.setVertexAttributeBuffers(gl, sceneObject.getDrawable());
		}
		
		if(CHECK_VALIDATION){ lastActiveObjectShader.checkValidateStatus(gl); } //[TODO]
		gl.drawArrays(WebGLRenderingContext.TRIANGLES, 0, 3*sceneObject.getDrawable().getTriCount());
	}
	
	@Override
	public void actionTerrainBegin(Terrain<WebGLRenderingContext,WGLBuffer,WGLTexture> terrain){
		
		//[TODO]
		/*
		//check if an object shader is bound 
		if(lastActiveObjectShader != null){ 
			lastActiveObjectShader.unuseShader(gl); 
			lastActiveObjectShader = null;
		}
		
		//bind and prepare generic terrain shader uniforms
		shaderTerrain.useShader(gl);
		shaderTerrain.setViewMatrices(gl, camera);
		
		shaderTerrain.setVertexAttributeBuffers(gl, terrain.getPositionBuffer());
		shaderTerrain.setTerrainMetrics(gl, terrain.getMetrics());
		
		//pass-specific shader uniforms
		shaderTerrain.setLightAndFog(gl, light, fogDensity);
		shaderTerrain.setMaterialLightingCoefficients(gl, terrain.getMaterial().getMaterial1());
		
		//bind necessary terrain textures
		terrain.getDataHeightAndAux().bind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);
		
		terrain.getDataNormalAndBlend().bind(gl, ShaderTerrainLightPass.NORMAL_AND_BLEND_TEXTURE_UNIT);
		terrain.getMaterial().getMaterial1().getTexColor().bind(gl, ShaderTerrainLightPass.COLOR1_TEXTURE_UNIT);
		terrain.getMaterial().getMaterial2().getTexColor().bind(gl, ShaderTerrainLightPass.COLOR2_TEXTURE_UNIT);
		*/
	}
	
	@Override
	public void actionTerrainTile(DrawableIBO<WebGLRenderingContext,WGLBuffer> tileIBO, Box<Vector2d> tileLerp) {
		
		//[TODO]
		/*
		shaderTerrain.setTileLerp(gl, tileLerp.getMin(), tileLerp.getMax());
		
		tileIBO.getIndexBuffer().bindAsElementArray(gl);
		
		if(CHECK_VALIDATION){ shaderTerrain.checkValidateStatus(gl); } //[TODO]
		gl.glDrawElements(WebGLRenderingContext.TRIANGLES, 3*tileIBO.getTriCount(), WebGLRenderingContext.UNSIGNED_SHORT, 0);
		*/
	}

	@Override
	public void actionTerrainEnd(Terrain<WebGLRenderingContext,WGLBuffer,WGLTexture> terrain) {
		
		//[TODO]
		/*
		shaderTerrain.unuseShader(gl);
		
		terrain.getDataHeightAndAux().unbind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);
		
		terrain.getDataNormalAndBlend().unbind(gl, ShaderTerrainLightPass.NORMAL_AND_BLEND_TEXTURE_UNIT);
		terrain.getMaterial().getMaterial1().getTexColor().unbind(gl, ShaderTerrainLightPass.COLOR1_TEXTURE_UNIT);
		terrain.getMaterial().getMaterial2().getTexColor().unbind(gl, ShaderTerrainLightPass.COLOR2_TEXTURE_UNIT);
		*/
	}
	
	protected boolean bindTexColor(WGLTexture texture){
		//bind texture if it's not already, first unbinding previous texture if there is one
		//returns false if it did not bind anything
		
		if(texture == lastActiveTexColor){ return false; }
		if(lastActiveTexColor != null){ lastActiveTexColor.unbind(gl, ShaderLightPass.COLOR_TEXTURE_UNIT); }
		
		texture.bind(gl, ShaderLightPass.COLOR_TEXTURE_UNIT);
		lastActiveTexColor = texture;
		return true;
	}
	
	protected boolean bindTexBump(WGLTexture texture){
		//bind texture if it's not already, first unbinding previous texture if there is one
		//returns false if it did not bind anything
		
		if(texture == lastActiveTexBump){ return false; }
		if(lastActiveTexBump != null){ lastActiveTexBump.unbind(gl, ShaderLightPass.BUMP_TEXTURE_UNIT); }
		
		texture.bind(gl, ShaderLightPass.BUMP_TEXTURE_UNIT);
		lastActiveTexColor = texture;
		return true;
	}
	
	protected boolean bindShader(){
		//bind shader if it's not already, first unbinding previous shader if there is one
		//returns false if it did not bind anything
		
		if(lastActiveObjectShader == shader){ return false; }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		shader.useShader(gl);
		shader.setViewMatrices(gl, camera);
		shader.setLightAndFog(gl, light, fogDensity);
		
		lastActiveObjectShader = shader;
		return true;
	}
	
	protected boolean bindShaderBump(){
		//bind shader if it's not already, first unbinding previous shader if there is one
		//returns false if it did not bind anything
		
		if(lastActiveObjectShader == shaderBump){ return false; }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		shaderBump.useShader(gl);
		shaderBump.setViewMatrices(gl, camera);
		shaderBump.setLightAndFog(gl, light, fogDensity);
		
		lastActiveObjectShader = shaderBump;
		return true;
	}
	
	protected void finishPass(){
		
		//unbind texture and shader, if there is one bound
		if(lastActiveTexColor != null){ lastActiveTexColor.unbind(gl, ShaderLightPass.COLOR_TEXTURE_UNIT); }
		if(lastActiveTexBump != null){ lastActiveTexBump.unbind(gl, ShaderLightPass.BUMP_TEXTURE_UNIT); }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		//unset transient data
		this.gl = null;
		light = null;
		camera = null;
	}
}
