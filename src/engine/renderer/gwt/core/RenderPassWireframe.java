package engine.renderer.gwt.core;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;

import engine.renderer.DrawableIBO;
import engine.renderer.gwt.object.WGLBuffer;
import engine.renderer.gwt.object.WGLTexture;
import engine.renderer.gwt.shader.ShaderProgram;
import engine.renderer.gwt.shader.lightpass.ShaderWireframe;
import engine.scene.SceneObject;
import engine.scene.SceneTraversal;
import engine.terrain.Terrain;
import geometry.math.Camera;
import geometry.math.Vector2d;
import geometry.spacepartition.Box;


public class RenderPassWireframe 
	extends SceneTraversal<WebGLRenderingContext,WGLBuffer,WGLTexture>{
	
	private ShaderWireframe shader;
	//private ShaderTerrainWireframe shaderTerrain;
	
	//transient data for a single pass
	protected WebGLRenderingContext gl;
	protected Camera camera;
	
	//things that can change; store last so not flipped every object
	protected ShaderProgram lastActiveObjectShader;
	
	
	public RenderPassWireframe(WebGLRenderingContext gl){
		shader = new ShaderWireframe(gl);
		//shaderTerrain = new ShaderTerrainWireframe(gl);
	}

	public void initializePass(WebGLRenderingContext gl, Camera camera){
		this.gl = gl;
		this.camera = camera;
		
		//no active shader
		lastActiveObjectShader = null;
		
		setupWireframePass();
	}
	
	private void setupWireframePass(){
		
		gl.disable(WebGLRenderingContext.DEPTH_TEST);
		
		gl.depthMask(false);
		gl.colorMask(true, true, true, false);
		
		gl.enable(WebGLRenderingContext.BLEND);
		gl.blendFunc(WebGLRenderingContext.SRC_ALPHA, WebGLRenderingContext.ONE_MINUS_SRC_ALPHA);
	}
	
	@Override
	public void actionObject(SceneObject<WebGLRenderingContext,WGLBuffer,WGLTexture> sceneObject){
		if(!sceneObject.getDrawable().hasWireframeData()){ return; }
		
		//bind shader, if necessary
		bindObjectShader();
				
		shader.setColor(gl, sceneObject.getDrawOptions().getWireframeColor());
		shader.setObjectTransformation(gl, sceneObject);
		shader.setVertexAttributeBuffers(gl, sceneObject.getDrawable());
		gl.drawArrays(WebGLRenderingContext.LINES, 0, 2*sceneObject.getDrawable().getEdgeCount());
	}
	
	@Override
	public void actionTerrainBegin(Terrain<WebGLRenderingContext,WGLBuffer,WGLTexture> terrain){
		
		//[TODO]
		/*
		//check if an object shader is bound 
		if(lastActiveObjectShader != null){ 
			lastActiveObjectShader.unuseShader(gl); 
			lastActiveObjectShader = null;
		}
		
		//bind and prepare generic terrain shader uniforms
		shaderTerrain.useShader(gl);
		shaderTerrain.setViewMatrices(gl, camera);
		
		shaderTerrain.setVertexAttributeBuffers(gl, terrain.getPositionBuffer());
		shaderTerrain.setTerrainMetrics(gl, terrain.getMetrics());
		
		shaderTerrain.setColor(gl, terrain.getDrawOptions().getWireframeColor());

		//bind necessary terrain textures
		terrain.getDataHeightAndAux().bind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);
		*/
	}
	
	@Override
	public void actionTerrainTile(DrawableIBO<WebGLRenderingContext,WGLBuffer> tileIBO, Box<Vector2d> tileLerp) {
		if(!tileIBO.hasWireframeData()){ return; }
		
		//[TODO]
		/*
		shaderTerrain.setTileLerp(gl, tileLerp.getMin(), tileLerp.getMax());
		
		tileIBO.getWireframeIndexBuffer().bindAsElementArray(gl);
		
		gl.glDrawElements(WebGLRenderingContext.LINES, 2*tileIBO.getEdgeCount(), WebGLRenderingContext.UNSIGNED_SHORT, 0);
		*/
	}

	@Override
	public void actionTerrainEnd(Terrain<WebGLRenderingContext,WGLBuffer,WGLTexture> terrain) {
		
		//[TODO]
		/*
		shaderTerrain.unuseShader(gl);
		
		terrain.getDataHeightAndAux().unbind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);
		*/
	}
	
	protected boolean bindObjectShader(){
		
		//check if already bound, or if another shader is bound and needs to be unbound 
		if(lastActiveObjectShader == shader){ return false; }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		shader.useShader(gl);
		shader.setViewMatrices(gl, camera);
		
		lastActiveObjectShader = shader;
		return true;
	}
	
	public void finishPass(){
		
		//unbind shader, if there is one bound
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		//unset transient data
		this.gl = null;
		camera = null;
	}
}
