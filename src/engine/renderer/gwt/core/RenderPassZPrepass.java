package engine.renderer.gwt.core;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;

import engine.renderer.DrawableIBO;
import engine.renderer.gwt.object.WGLBuffer;
import engine.renderer.gwt.object.WGLTexture;
import engine.renderer.gwt.shader.ShaderProgram;
import engine.renderer.gwt.shader.lightpass.ShaderZPrepass;
import engine.scene.SceneObject;
import engine.scene.SceneTraversal;
import engine.terrain.Terrain;
import geometry.math.Camera;
import geometry.math.Vector2d;
import geometry.math.Vector3d;
import geometry.spacepartition.Box;


public class RenderPassZPrepass 
	extends SceneTraversal<WebGLRenderingContext,WGLBuffer,WGLTexture>{

	private ShaderZPrepass shaderAlpha;
	private ShaderZPrepass shaderNoAlpha;
	//private ShaderTerrainZPrepass shaderTerrain;
	
	//transient data for a single pass
	protected WebGLRenderingContext gl;
	protected Camera camera;
	protected float fogDensity;
	protected Vector3d fogColor;
	protected boolean translucentPass;
	
	
	//things that can change; store last so not flipped every object
	protected WGLTexture lastActiveTexColor;
	protected ShaderProgram lastActiveObjectShader;
	
	
	public RenderPassZPrepass(WebGLRenderingContext gl){
		shaderAlpha = new ShaderZPrepass(gl, true);
		shaderNoAlpha = new ShaderZPrepass(gl, false);
		//shaderTerrain = new ShaderTerrainZPrepass(gl);
	}
	
	public void initializePass(WebGLRenderingContext gl, Camera camera, float fogDensity, Vector3d fogColor, boolean translucentPass){
		this.gl = gl;
		this.camera = camera;
		this.fogDensity = fogDensity;
		this.fogColor = fogColor;
		this.translucentPass = translucentPass;
		
		//no active texture
		lastActiveTexColor = null;
		lastActiveObjectShader = null;
		
		setupZPrepass();
	}
	
	private void setupZPrepass(){
		
		gl.enable(WebGLRenderingContext.DEPTH_TEST);
		gl.depthFunc(WebGLRenderingContext.LESS);
		
		gl.depthMask(true);
		gl.colorMask(true, true, true, true);
		
		gl.disable(WebGLRenderingContext.BLEND);
		
		//gl.enable(WebGLRenderingContext.CULL_FACE);
		//gl.cullFace(WebGLRenderingContext.BACK);
	}

	@Override
	public void actionObject(SceneObject<WebGLRenderingContext,WGLBuffer,WGLTexture> sceneObject){
		
		if(translucentPass || sceneObject.getMaterial().hasAlphaMask()){
			bindShaderAlpha();
			bindTexColor(sceneObject.getMaterial().getTexColor());
			
			shaderAlpha.setOpacity(gl, sceneObject.getDrawOptions().getOpacity());
			shaderAlpha.setObjectTransformation(gl, sceneObject);
			shaderAlpha.setVertexAttributeBuffers(gl, sceneObject.getDrawable());
		}else{
			bindShaderNoAlpha();
			
			shaderNoAlpha.setObjectTransformation(gl, sceneObject);
			shaderNoAlpha.setVertexAttributeBuffers(gl, sceneObject.getDrawable());
		}

		gl.drawArrays(WebGLRenderingContext.TRIANGLES, 0, 3*sceneObject.getDrawable().getTriCount());
	}
	
	@Override
	public void actionTerrainBegin(Terrain<WebGLRenderingContext,WGLBuffer,WGLTexture> terrain){
		
		//[TODO]
		/*
		//check if an object shader is bound 
		if(lastActiveObjectShader != null){ 
			lastActiveObjectShader.unuseShader(gl); 
			lastActiveObjectShader = null;
		}
		
		//bind and prepare generic terrain shader uniforms
		shaderTerrain.useShader(gl);
		shaderTerrain.setViewMatrices(gl, camera);
		
		shaderTerrain.setVertexAttributeBuffers(gl, terrain.getPositionBuffer());
		shaderTerrain.setTerrainMetrics(gl, terrain.getMetrics());
		
		//pass-specific shader uniforms
		shaderTerrain.setOpacity(gl, terrain.getDrawOptions().getOpacity());
		shaderTerrain.setFog(gl, fogColor, fogDensity);
		
		//bind necessary terrain textures
		terrain.getDataHeightAndAux().bind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);
		*/
	}
	
	@Override
	public void actionTerrainTile(DrawableIBO<WebGLRenderingContext,WGLBuffer> tileIBO, Box<Vector2d> tileLerp) {
		
		//[TODO]
		/*
		shaderTerrain.setTileLerp(gl, tileLerp.getMin(), tileLerp.getMax());
		
		tileIBO.getIndexBuffer().bindAsElementArray(gl);
		
		gl.glDrawElements(WebGLRenderingContext.TRIANGLES, 3*tileIBO.getTriCount(), WebGLRenderingContext.UNSIGNED_SHORT, 0);
		*/
	}

	@Override
	public void actionTerrainEnd(Terrain<WebGLRenderingContext,WGLBuffer,WGLTexture> terrain) {
		
		//[TODO]
		/*
		shaderTerrain.unuseShader(gl);
		
		terrain.getDataHeightAndAux().unbind(gl, ShaderTerrainGeneric.HEIGHT_AND_AUX_TEXTURE_UNIT);
		*/
	}
	
	protected boolean bindTexColor(WGLTexture texture){
		//bind texture if it's not already, first unbinding previous texture if there is one
		//returns false if it did not bind anything
		
		if(texture == lastActiveTexColor){ return false; }
		if(lastActiveTexColor != null){ lastActiveTexColor.unbind(gl, ShaderZPrepass.COLOR_TEXTURE_UNIT); }
		
		texture.bind(gl, ShaderZPrepass.COLOR_TEXTURE_UNIT);
		lastActiveTexColor = texture;
		return true;
	}
	
	protected boolean bindShaderAlpha(){
		
		//check if already bound, or if another shader is bound and needs to be unbound 
		if(lastActiveObjectShader == shaderAlpha){ return false; }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		shaderAlpha.useShader(gl);
		shaderAlpha.setViewMatrices(gl, camera);
		
		shaderAlpha.setFog(gl, fogColor, fogDensity);
		
		lastActiveObjectShader = shaderAlpha;
		return true;
	}
	
	protected boolean bindShaderNoAlpha(){
		
		//check if already bound, or if another shader is bound and needs to be unbound 
		if(lastActiveObjectShader == shaderNoAlpha){ return false; }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		shaderNoAlpha.useShader(gl);
		shaderNoAlpha.setViewMatrices(gl, camera);
		
		shaderNoAlpha.setFog(gl, fogColor, fogDensity);
		
		lastActiveObjectShader = shaderNoAlpha;
		return true;
	}
	
	public void finishPass(){
		
		//unbind texture and shader, if there is one bound
		if(lastActiveTexColor != null){ lastActiveTexColor.unbind(gl, ShaderZPrepass.COLOR_TEXTURE_UNIT); }
		if(lastActiveObjectShader != null){ lastActiveObjectShader.unuseShader(gl); }
		
		//unset transient data
		this.gl = null;
		camera = null;
		fogColor = null;
	}
}
