package engine.renderer.gwt.object;

import com.google.gwt.event.dom.client.LoadEvent;
import com.google.gwt.event.dom.client.LoadHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootPanel;
import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLTexture;

import engine.renderer.GenericTexture;
import engine.toolsAWT.BufferTexture;
import geometry.common.MessageOutput;


public class WGLTexture extends GenericTexture<WebGLRenderingContext>{

	private int width, height;
	private WebGLTexture tex;
	
	
	public WGLTexture(WebGLTexture tex, int width, int height){
		
		this.tex = tex;
		this.width = width;
		this.height = height;
	}
	
	
	public WGLTexture(final WebGLRenderingContext gl, ImageResource imgRes, 
		final boolean generateMipmaps, final boolean wrap){

		//generate textures
		tex = gl.createTexture();
		
		//initialize the texture
		final Image img = new Image();
		img.addLoadHandler(new LoadHandler(){
			@Override 
			public void onLoad(LoadEvent event){
				RootPanel.get().remove(img);
				initializeTexture(gl, img, generateMipmaps, wrap);
			}
		});
		img.setVisible(false);
		RootPanel.get().add(img);
		img.setUrl(imgRes.getSafeUri());
	}
	
	@Override
	public void delete(WebGLRenderingContext gl){
		
		gl.deleteTexture(tex);
		
		MessageOutput.printDebug("Deleted texture");
	}
	
	private void initializeTexture(WebGLRenderingContext gl, Image img, 
		boolean generateMipmaps, boolean wrap){
		
		MessageOutput.printDebug("Initializing texture ...");
				
		//store width and height
		this.width = img.getWidth();
		this.height = img.getHeight();
		
		gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, tex);
		
		gl.pixelStorei(WebGLRenderingContext.UNPACK_FLIP_Y_WEBGL, 1);
		
		gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_WRAP_S, wrap ? WebGLRenderingContext.REPEAT : WebGLRenderingContext.CLAMP_TO_EDGE);
		gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_WRAP_T, wrap ? WebGLRenderingContext.REPEAT : WebGLRenderingContext.CLAMP_TO_EDGE);
		gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_MAG_FILTER, WebGLRenderingContext.LINEAR);
		gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_MIN_FILTER, 
			generateMipmaps ? WebGLRenderingContext.LINEAR_MIPMAP_LINEAR : WebGLRenderingContext.LINEAR);
		
		gl.texImage2D(WebGLRenderingContext.TEXTURE_2D, 0, WebGLRenderingContext.RGBA, WebGLRenderingContext.RGBA, WebGLRenderingContext.UNSIGNED_BYTE, img.getElement());
		if(generateMipmaps){ gl.generateMipmap(WebGLRenderingContext.TEXTURE_2D); }
		
		gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, null);
	}
	
	@Override
	public int getWidth(){ return width; }
	
	@Override
	public int getHeight(){ return height; }
	
	
	public void bind(WebGLRenderingContext gl, int texUnitColor){

		gl.activeTexture(WebGLRenderingContext.TEXTURE0+texUnitColor);
		gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, tex);
	}
	
	public void unbind(WebGLRenderingContext gl, int texUnitColor){
		
		gl.activeTexture(WebGLRenderingContext.TEXTURE0+texUnitColor);
		gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, null);
	}

	public void bindReadFrom(WebGLRenderingContext gl){
		
		//attach texture
		gl.framebufferTexture2D(WebGLRenderingContext.FRAMEBUFFER, 
			WebGLRenderingContext.COLOR_ATTACHMENT0, WebGLRenderingContext.TEXTURE_2D, tex, 0);
		
		//specify drawbuffers and readbuffers
		//TODO: not necessary on GLES2?
		//gl.DrawBuffer(WebGLRenderingContext.NONE);
		//gl.ReadBuffer(WebGLRenderingContext.COLOR_ATTACHMENT0); 
		
		//check if it worked
		int status = gl.checkFramebufferStatus(WebGLRenderingContext.FRAMEBUFFER);
		if(status != WebGLRenderingContext.FRAMEBUFFER_COMPLETE){ 
			throw new Error("FBO status is not GL_FRAMEBUFFER_COMPLETE."); }
		
		//set appropriate viewport
		gl.viewport(0, 0, width, height);
	}
	
	public void unbindReadFrom(WebGLRenderingContext gl){
		
		//detach texture
		gl.framebufferTexture2D(WebGLRenderingContext.FRAMEBUFFER, 
			WebGLRenderingContext.COLOR_ATTACHMENT0, WebGLRenderingContext.TEXTURE_2D, null, 0);
	}
	
	/**
	 * Uses glReadPixels, which is slow.  Only use this if it is really necessary.
	 */
	@Override
	public void getAsBufferTexture(WebGLRenderingContext gl, BufferTexture target, 
		int srcMinX, int srcMinY, int srcWidth, int srcHeight){
		
		/*//[TODO] can do e.g. gl.glPixelStorei(GL.GL_PACK_ALIGNMENT, 1) instead, 
		//but maybe there is a reason not to; is it slower?
		srcWidth += (4 - (srcWidth % 4)) % 4;
		srcHeight += (4 - (srcHeight % 4)) % 4;
		
		//get buffer
		ByteBuffer data = target.getBufferForWriting(srcWidth, srcHeight);
		
		//read in the rect of pixel data
		bindReadFrom(gl);
		gl.ReadPixels(srcMinX, srcMinY, srcWidth, srcHeight, 
			GLES11Ext.GL_BGRA, WebGLRenderingContext.UNSIGNED_BYTE, data);
		unbindReadFrom(gl);*/
	}
	
	/**
	 * Uses glReadPixels, which is slow.  Only use this if it is really necessary.
	 */
	@Override
	public void getAsBufferTexture(WebGLRenderingContext gl, BufferTexture target, 
		int srcMinX, int srcMinY, int srcWidth, int srcHeight,
		int targetScaledWidth, int targetScaledHeight){
		
		/*//find appropriate scaling
		float sx = (srcWidth <= targetScaledWidth) ? 1.0f : (float)targetScaledWidth/(float)srcWidth;
		float sy = (srcHeight <= targetScaledHeight) ? 1.0f : (float)targetScaledHeight/(float)srcHeight;
		float s = (sx < sy) ? sx : sy;
		
		int scaledWidth = (int)(srcWidth * s);
		int scaledHeight = (int)(srcHeight * s);
		
		//ensure target size has dims multiple of 4
		scaledWidth += (4 - (scaledWidth % 4)) % 4;
		scaledHeight += (4 - (scaledHeight % 4)) % 4;
		
		//create stuff necessary to draw texture onto a BufferColor at requested size
		BufferColor bufThumb = new BufferColor(gl, scaledWidth, scaledHeight);
		Quad<GLES20,GLESBuffer> quad = new Quad<GLES20,GLESBuffer>(gl, GLESBuffer.getConstructor());
		ShaderQuad shader = new ShaderQuad(gl, "Copy quad shader");
		
		//prepare bufThumb for drawing to
		bufThumb.clearColor(gl, new Vector4d(1,0,1,1));
		bufThumb.bindDrawTo(gl);
		//draw this texture onto bufThumb (this is exactly drawBuffer in main Renderer routine)
		shader.useShader(gl);
		shader.setVertexAttributeBuffers(gl, quad);
		bind(gl, ShaderQuad.COLOR_TEXTURE_UNIT);
		gl.DrawArrays(WebGLRenderingContext.TRIANGLES, 0, 3*quad.getTriCount());
		unbind(gl, ShaderQuad.COLOR_TEXTURE_UNIT);
		shader.unuseShader(gl);
		//unbind bufThumb
		bufThumb.unbindDrawTo(gl);
		
		//read bufThumb into target BufferTexture
		ByteBuffer data = target.getBufferForWriting(scaledWidth, scaledHeight);
		
		bufThumb.bindReadFrom(gl);
		gl.ReadPixels(0, 0, scaledWidth, scaledHeight, 
			GLES11Ext.GL_BGRA, WebGLRenderingContext.UNSIGNED_BYTE, data);
		bufThumb.unbindReadFrom(gl);
		
		//delete created stuff
		bufThumb.delete(gl);
		//[TODO] delete quad
		//[TODO] delete shader*/
	}
}