package engine.renderer.gwt.object;

import com.googlecode.gwtgl.array.Float32Array;
import com.googlecode.gwtgl.array.Uint16Array;
import com.googlecode.gwtgl.binding.WebGLBuffer;
import com.googlecode.gwtgl.binding.WebGLRenderingContext;

import engine.renderer.GenericBuffer;
import geometry.common.MessageOutput;


public class WGLBuffer extends GenericBuffer<WebGLRenderingContext>{

	//private static final int BYTES_PER_SHORT = 2;
	//private static final int BYTES_PER_FLOAT = 4;
	
	private WebGLBuffer buffer;
	private int floatsPerAttribute; //only used if created as vertex attribute buffer
	
	//constructor
	public static class Constructor implements BufferConstructor<WebGLRenderingContext, WGLBuffer>{
		
		@Override
		public WGLBuffer construct(WebGLRenderingContext context, float[] fArray, int floatsPerAttribute){
			return new WGLBuffer(context, fArray, floatsPerAttribute);
		}
		
		@Override
		public WGLBuffer construct(WebGLRenderingContext context, int[] iArray){
			return new WGLBuffer(context, iArray);
		}
	}
	protected static Constructor constructor = new Constructor();
	public static Constructor getConstructor(){ return constructor; }
	
	
	public WGLBuffer(WebGLRenderingContext gl, float[] fArray, int floatsPerAttribute){
	
		this.floatsPerAttribute = floatsPerAttribute;
		buffer = createAndFillVertexAttribBuffer(gl, fArray);
	}
	
	public WGLBuffer(WebGLRenderingContext gl, int[] iArray){
		
		buffer = createAndFillElementArrayBuffer(gl, iArray);
	}
	
	private static WebGLBuffer createAndFillVertexAttribBuffer(WebGLRenderingContext gl, float[] fArray){
		
		WebGLBuffer h = gl.createBuffer();
		gl.bindBuffer(WebGLRenderingContext.ARRAY_BUFFER, h);
		gl.bufferData(WebGLRenderingContext.ARRAY_BUFFER, Float32Array.create(fArray), WebGLRenderingContext.STATIC_DRAW);
				
		return h;
	}
	
	private static WebGLBuffer createAndFillElementArrayBuffer(WebGLRenderingContext gl, int[] iArray){
		
		WebGLBuffer h = gl.createBuffer();
		gl.bindBuffer(WebGLRenderingContext.ELEMENT_ARRAY_BUFFER, h);
		gl.bufferData(WebGLRenderingContext.ELEMENT_ARRAY_BUFFER, Uint16Array.create(iArray), WebGLRenderingContext.STATIC_DRAW);
		
		return h;
	}
	
	//[TODO] buffer modification
	//public void modify(GL2 gl, long offset, long size, Buffer data){
	//	
	//	gl.glBufferSubData(bufferID, offset, size, data);
	//}
	
	@Override
	public void delete(WebGLRenderingContext gl){
		
		gl.deleteBuffer(buffer);
		
		MessageOutput.printDebug("Deleted array buffer");
	}
	
	//bind
	public void bindAsElementArray(WebGLRenderingContext gl){
		
		gl.bindBuffer(WebGLRenderingContext.ELEMENT_ARRAY_BUFFER, buffer);
	}
	
	public void bindAsVertexAttrib(WebGLRenderingContext gl, int vertexAttributeIndex){ 
		
		gl.bindBuffer(WebGLRenderingContext.ARRAY_BUFFER, buffer);
		gl.vertexAttribPointer(vertexAttributeIndex, floatsPerAttribute, WebGLRenderingContext.FLOAT, false, 0, 0);
	}
}