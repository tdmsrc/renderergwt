package engine.renderer.gwt.shader;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.TextResource;

public interface RShaders extends ClientBundle{

	public static RShaders INSTANCE = GWT.create(RShaders.class);
	public static final String resPath = "resource/shader/";

	@Source(value = { resPath + "lightpass/BumpedNormalNoneFrag.glsl" })
	TextResource bumpedNormalNoneFrag();
	
	@Source(value = { resPath + "lightpass/BumpedNormalTextureFrag.glsl" })
	TextResource bumpedNormalTextureFrag();
	
	@Source(value = { resPath + "lightpass/FogIntensity.glsl" })
	TextResource fogIntensity();
	
	@Source(value = { resPath + "lightpass/IsShadowedCube.glsl" })
	TextResource isShadowedCube();
	
	@Source(value = { resPath + "lightpass/IsShadowedDirectional.glsl" })
	TextResource isShadowedDirectional();
	
	@Source(value = { resPath + "lightpass/IsShadowedNever.glsl" })
	TextResource isShadowedNever();
	
	@Source(value = { resPath + "lightpass/ObjRotation.glsl" })
	TextResource objRotation();
	
	@Source(value = { resPath + "lightpass/ObjRotationBillboard.glsl" })
	TextResource objRotationBillboard();
	
	@Source(value = { resPath + "lightpass/ParallaxMapping.glsl" })
	TextResource parallaxMapping();
	
	@Source(value = { resPath + "lightpass/ParallaxNone.glsl" })
	TextResource parallaxNone();
	
	@Source(value = { resPath + "lightpass/ParallaxOcclusionMapping.glsl" })
	TextResource parallaxOcclusionMapping();
	
	@Source(value = { resPath + "lightpass/PhongFunction.glsl" })
	TextResource phongFunction();
	
	@Source(value = { resPath + "lightpass/ShaderAmbientAndEmissiveFrag.glsl" })
	TextResource shaderAmbientAndEmissiveFrag();
	
	@Source(value = { resPath + "lightpass/ShaderAmbientAndEmissiveVert.glsl" })
	TextResource shaderAmbientAndEmissiveVert();
	
	@Source(value = { resPath + "lightpass/ShaderLightPassFrag.glsl" })
	TextResource shaderLightPassFrag();
	
	@Source(value = { resPath + "lightpass/ShaderLightPassVert.glsl" })
	TextResource shaderLightPassVert();
	
	@Source(value = { resPath + "lightpass/ShaderShadowMapMaskFrag.glsl" })
	TextResource shaderShadowMapMaskFrag();
	
	@Source(value = { resPath + "lightpass/ShaderShadowMapMaskVert.glsl" })
	TextResource shaderShadowMapMaskVert();
	
	@Source(value = { resPath + "lightpass/ShaderShadowMapNoMaskFrag.glsl" })
	TextResource shaderShadowMapNoMaskFrag();
	
	@Source(value = { resPath + "lightpass/ShaderShadowMapNoMaskVert.glsl" })
	TextResource shaderShadowMapNoMaskVert();
	
	@Source(value = { resPath + "lightpass/ShaderWireframeFrag.glsl" })
	TextResource shaderWireframeFrag();
	
	@Source(value = { resPath + "lightpass/ShaderWireframeVert.glsl" })
	TextResource shaderWireframeVert();
	
	@Source(value = { resPath + "lightpass/ShaderZPrepassAlphaFrag.glsl" })
	TextResource shaderZPrepassAlphaFrag();
	
	@Source(value = { resPath + "lightpass/ShaderZPrepassAlphaVert.glsl" })
	TextResource shaderZPrepassAlphaVert();
	
	@Source(value = { resPath + "lightpass/ShaderZPrepassNoAlphaFrag.glsl" })
	TextResource shaderZPrepassNoAlphaFrag();
	
	@Source(value = { resPath + "lightpass/ShaderZPrepassNoAlphaVert.glsl" })
	TextResource shaderZPrepassNoAlphaVert();
	
	@Source(value = { resPath + "lightpass/ShadowFilterNone.glsl" })
	TextResource shadowFilterNone();
	
	@Source(value = { resPath + "lightpass/ShadowFilterPCF.glsl" })
	TextResource shadowFilterPCF();
	
	@Source(value = { resPath + "lightpass/VaryingTangentsNoneVert.glsl" })
	TextResource varyingTangentsNoneVert();
	
	@Source(value = { resPath + "lightpass/VaryingTangentsTextureVert.glsl" })
	TextResource varyingTangentsTextureVert();
	
	@Source(value = { resPath + "quad/ShaderQuadFragBlurH.glsl" })
	TextResource shaderQuadFragBlurH();

	@Source(value = { resPath + "quad/ShaderQuadFragBlurRadial.glsl" })
	TextResource shaderQuadFragBlurRadial();

	@Source(value = { resPath + "quad/ShaderQuadFragBlurV.glsl" })
	TextResource shaderQuadFragBlurV();

	@Source(value = { resPath + "quad/ShaderQuadFragColorAdjust.glsl" })
	TextResource shaderQuadFragColorAdjust();

	@Source(value = { resPath + "quad/ShaderQuadFragCopy.glsl" })
	TextResource shaderQuadFragCopy();

	@Source(value = { resPath + "quad/ShaderQuadFragCopyAlpha.glsl" })
	TextResource shaderQuadFragCopyAlpha();

	@Source(value = { resPath + "quad/ShaderQuadFragDepthOfField.glsl" })
	TextResource shaderQuadFragDepthOfField();

	@Source(value = { resPath + "quad/ShaderQuadFragHSVAdjust.glsl" })
	TextResource shaderQuadFragHSVAdjust();

	@Source(value = { resPath + "quad/ShaderQuadFragSSAO.glsl" })
	TextResource shaderQuadFragSSAO();

	@Source(value = { resPath + "quad/ShaderQuadVert.glsl" })
	TextResource shaderQuadVert();
	
	@Source(value = { resPath + "skybox/ShaderSkyboxFrag.glsl" })
	TextResource shaderSkyboxFrag();

	@Source(value = { resPath + "skybox/ShaderSkyboxVert.glsl" })
	TextResource shaderSkyboxVert();
	
	@Source(value = { resPath + "terrain/GenericTerrainVert.glsl" })
	TextResource genericTerrainVert();

	@Source(value = { resPath + "terrain/ShaderTerrainAmbientAndEmissiveFrag.glsl" })
	TextResource shaderTerrainAmbientAndEmissiveFrag();

	@Source(value = { resPath + "terrain/ShaderTerrainAmbientAndEmissiveVert.glsl" })
	TextResource shaderTerrainAmbientAndEmissiveVert();

	@Source(value = { resPath + "terrain/ShaderTerrainLightPassFrag.glsl" })
	TextResource shaderTerrainLightPassFrag();

	@Source(value = { resPath + "terrain/ShaderTerrainLightPassVert.glsl" })
	TextResource shaderTerrainLightPassVert();

	@Source(value = { resPath + "terrain/ShaderTerrainShadowMapFrag.glsl" })
	TextResource shaderTerrainShadowMapFrag();

	@Source(value = { resPath + "terrain/ShaderTerrainShadowMapVert.glsl" })
	TextResource shaderTerrainShadowMapVert();

	@Source(value = { resPath + "terrain/ShaderTerrainWireframeFrag.glsl" })
	TextResource shaderTerrainWireframeFrag();

	@Source(value = { resPath + "terrain/ShaderTerrainWireframeVert.glsl" })
	TextResource shaderTerrainWireframeVert();

	@Source(value = { resPath + "terrain/ShaderTerrainZPrepassFrag.glsl" })
	TextResource shaderTerrainZPrepassFrag();

	@Source(value = { resPath + "terrain/ShaderTerrainZPrepassVert.glsl" })
	TextResource shaderTerrainZPrepassVert();
}
