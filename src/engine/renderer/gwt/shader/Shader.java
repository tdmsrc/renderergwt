package engine.renderer.gwt.shader;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLShader;

import geometry.common.MessageOutput;


public abstract class Shader{
	protected static final boolean CHECK_ERRORS = true;
	
	protected WebGLShader shader;
	
	
	protected Shader(WebGLShader shader){
		this.shader = shader;
	}
	
	public WebGLShader getWebGLShader(){ return shader; }

	
	public void checkCompileStatus(WebGLRenderingContext gl){
		
        boolean status = gl.getShaderParameterb(shader, WebGLRenderingContext.COMPILE_STATUS);
        
        if(status == false){
        	MessageOutput.printDebug("GL_COMPILE_STATUS is GL_FALSE! Info log: ");
        	throw new Error(getShaderInfoLog(gl));
        }else{
        	MessageOutput.printDebug("GL_COMPILE_STATUS is ok. Info log: \n" + getShaderInfoLog(gl));
        }
    }
	
	public String getShaderInfoLog(WebGLRenderingContext gl){
		
        String infoLog = gl.getShaderInfoLog(shader);
        
        return infoLog;
    }

}
