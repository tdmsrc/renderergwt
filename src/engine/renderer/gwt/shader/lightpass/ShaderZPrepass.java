package engine.renderer.gwt.shader.lightpass;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLUniformLocation;

import engine.renderer.Drawable;
import engine.renderer.gwt.object.WGLBuffer;
import engine.renderer.gwt.shader.RShaders;
import geometry.math.Vector3d;


public class ShaderZPrepass extends ShaderGeneric{
	
	public static final int COLOR_TEXTURE_UNIT = 1; //[TODO] only used if useAlpha == true
	
	public static final int 
		VERTEX_POSITION_ATTRIBUTE = 0,
		VERTEX_TEX_ATTRIBUTE = 1; //[TODO] only used if useAlpha == true
	
	//texture/material data
	private WebGLUniformLocation textureColorUniform; //[TODO] only used if useAlpha == true
	private WebGLUniformLocation opacityUniform; //[TODO] only used if useAlpha == true
	//fog color
	private WebGLUniformLocation fogColorUniform, fogDensityUniform;
	
	//shader options
	private boolean useAlpha; 
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderZPrepass(WebGLRenderingContext gl, boolean useAlpha){
		super(gl, "Z-Prepass shader [useAlpha: " + useAlpha + "]");
		
		this.useAlpha = useAlpha;
		
		addVertexShaderSourceIdentifier("getFogIntensity", RShaders.INSTANCE.fogIntensity().getText());
		
		if(useAlpha){
			setPrimaryVertexShaderSource(RShaders.INSTANCE.shaderZPrepassAlphaVert().getText());
			setPrimaryFragmentShaderSource(RShaders.INSTANCE.shaderZPrepassAlphaFrag().getText());
		}else{
			setPrimaryVertexShaderSource(RShaders.INSTANCE.shaderZPrepassNoAlphaVert().getText());
			setPrimaryFragmentShaderSource(RShaders.INSTANCE.shaderZPrepassNoAlphaFrag().getText());
		}
		
		finishAttaching(gl);
	}
	
	@Override
	protected void setVertexAttributes(WebGLRenderingContext gl) {

		addVertexAttribute(gl, "vertexPosition", VERTEX_POSITION_ATTRIBUTE);
		if(useAlpha){
			addVertexAttribute(gl, "vertexTex", VERTEX_TEX_ATTRIBUTE);
		}
	}

	@Override
	protected void setUniforms(WebGLRenderingContext gl) {
		super.setUniforms(gl);

		if(useAlpha){
			textureColorUniform = gl.getUniformLocation(shaderProgram, "texColor");
			opacityUniform = gl.getUniformLocation(shaderProgram, "opacity");
		}
		
	    fogColorUniform = gl.getUniformLocation(shaderProgram, "fogColor");
	    fogDensityUniform = gl.getUniformLocation(shaderProgram, "fogDensity");
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(WebGLRenderingContext gl){
		super.useShader(gl);

		//set texture
		if(useAlpha){
			gl.uniform1i(textureColorUniform, COLOR_TEXTURE_UNIT);
		}
	}
	
	public void setOpacity(WebGLRenderingContext gl, float opacity){
		
		//set opacity
		gl.uniform1f(opacityUniform, opacity);
	}

	public void setFog(WebGLRenderingContext gl, Vector3d fogColor, float fogDensity){
		
		//set fog color uniform
		gl.uniform3f(fogColorUniform, fogColor.getX(), fogColor.getY(), fogColor.getZ());
		gl.uniform1f(fogDensityUniform, fogDensity);
	}
	
	public void setVertexAttributeBuffers(WebGLRenderingContext gl, Drawable<?,WGLBuffer> drawable){
		
		//bind vertex data
		drawable.getPositionBuffer().bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
		if(useAlpha){
			drawable.getTexBuffer().bindAsVertexAttrib(gl, VERTEX_TEX_ATTRIBUTE);
		}
	}
}
