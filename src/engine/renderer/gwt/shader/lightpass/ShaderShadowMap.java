package engine.renderer.gwt.shader.lightpass;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLUniformLocation;

import engine.renderer.Drawable;
import engine.renderer.gwt.object.WGLBuffer;
import engine.renderer.gwt.shader.RShaders;


public class ShaderShadowMap extends ShaderGeneric{
	
	public static final int COLOR_TEXTURE_UNIT = 1; //[TODO] only used if alphaMasking == true
	
	public static final int 
		VERTEX_POSITION_ATTRIBUTE = 0,
		VERTEX_TEX_ATTRIBUTE = 1; //[TODO] only used if alphaMasking == true
	
	//texture/material data
	private WebGLUniformLocation textureColorUniform; //[TODO] only used if alphaMasking == true
	
	//shader options
	private boolean alphaMasking;

	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderShadowMap(WebGLRenderingContext gl, boolean alphaMasking){
		super(gl, "Shadow map shader [alphaMasking: " + alphaMasking + "]");
		
		this.alphaMasking = alphaMasking;
		
		if(alphaMasking){
			setPrimaryVertexShaderSource(RShaders.INSTANCE.shaderShadowMapMaskVert().getText());
			setPrimaryFragmentShaderSource(RShaders.INSTANCE.shaderShadowMapMaskFrag().getText());
		}else{
			setPrimaryVertexShaderSource(RShaders.INSTANCE.shaderShadowMapNoMaskVert().getText());
			setPrimaryFragmentShaderSource(RShaders.INSTANCE.shaderShadowMapNoMaskFrag().getText());
		}
		
		finishAttaching(gl);
	}
	
	@Override
	protected void setVertexAttributes(WebGLRenderingContext gl){
	
		addVertexAttribute(gl, "vertexPosition", VERTEX_POSITION_ATTRIBUTE);
		if(alphaMasking){
			addVertexAttribute(gl, "vertexTex", VERTEX_TEX_ATTRIBUTE);
		}
	}

	@Override
	protected void setUniforms(WebGLRenderingContext gl) {
		super.setUniforms(gl);
		
		if(alphaMasking){
			textureColorUniform = gl.getUniformLocation(shaderProgram, "texColor");
		}
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(WebGLRenderingContext gl){
		super.useShader(gl);
		
		//set texture
		if(alphaMasking){
			gl.uniform1i(textureColorUniform, COLOR_TEXTURE_UNIT);
		}
	}
	
	public void setVertexAttributeBuffers(WebGLRenderingContext gl, Drawable<?,WGLBuffer> drawable){
		
		//bind vertex data
		drawable.getPositionBuffer().bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
		if(alphaMasking){
			drawable.getTexBuffer().bindAsVertexAttrib(gl, VERTEX_TEX_ATTRIBUTE);
		}
	}
}
