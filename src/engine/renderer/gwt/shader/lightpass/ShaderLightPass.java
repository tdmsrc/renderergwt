package engine.renderer.gwt.shader.lightpass;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLUniformLocation;

import engine.renderer.Drawable;
import engine.renderer.Material;
import engine.renderer.gwt.object.WGLBuffer;
import engine.renderer.gwt.shader.RShaders;
import engine.scene.Light;
import geometry.math.Vector3d;


public class ShaderLightPass extends ShaderGeneric{
	
	public static final int COLOR_TEXTURE_UNIT = 1;
	public static final int BUMP_TEXTURE_UNIT = 2; //[TODO] only used if normalMapping == true
	
	public static final int 
		VERTEX_POSITION_ATTRIBUTE = 0,
		VERTEX_TEX_ATTRIBUTE = 1,
		VERTEX_NORMAL_ATTRIBUTE = 2,
		VERTEX_TANGENT_S_ATTRIBUTE = 3, //[TODO] only used if normalMapping == true
		VERTEX_TANGENT_T_ATTRIBUTE = 4; //[TODO] only used if normalMapping == true
	
	//options for attaching shaders
	private static final OptionShadowFilter optionShadowFilter = OptionShadowFilter.PERCENTAGE_CLOSER_FILTERING;
	private static final OptionParallax optionParallax = OptionParallax.PARALLAX_OCCLUSION_MAPPING;
	
	//texture/material data
	private WebGLUniformLocation textureColorUniform;
	private WebGLUniformLocation textureBumpUniform; //[TODO] only used if normalMapping == true
	private WebGLUniformLocation reliefMappingHeightUniform; //[TODO] only used if normalMapping == true
	private WebGLUniformLocation diffuseCoefficientUniform, specularCoefficientUniform, specularExponentUniform;
	//light options
	private WebGLUniformLocation lightPositionUniform, lightColorUniform, lightAttenuationUniform;
	private WebGLUniformLocation fogDensityUniform;
	
	//shader options
	private boolean normalMapping;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderLightPass(WebGLRenderingContext gl, boolean normalMapping){
		super(gl, "Light pass shader [normalMapping: " + normalMapping + "]");
		
		this.normalMapping = normalMapping;
		
		setPrimaryVertexShaderSource(RShaders.INSTANCE.shaderLightPassVert().getText());
		setPrimaryFragmentShaderSource(RShaders.INSTANCE.shaderLightPassFrag().getText());
		
		addVertexShaderSourceIdentifier("getFogIntensity", RShaders.INSTANCE.fogIntensity().getText());
		addFragmentShaderSourceIdentifier("getPhong", RShaders.INSTANCE.phongFunction().getText());
		
		switch(optionShadowFilter){
		case NONE:
			addFragmentShaderSourceIdentifier("shadowFilter", RShaders.INSTANCE.shadowFilterNone().getText()); break;
		case PERCENTAGE_CLOSER_FILTERING:
			addFragmentShaderSourceIdentifier("shadowFilter", RShaders.INSTANCE.shadowFilterPCF().getText()); break;
		}
		
		attachShadowShaders(gl);
		
		if(normalMapping){
			addVertexShaderSourceIdentifier("setTangentVectors", RShaders.INSTANCE.varyingTangentsTextureVert().getText());
			addFragmentShaderSourceIdentifier("getBumpedNormal", RShaders.INSTANCE.bumpedNormalTextureFrag().getText());
						
			switch(optionParallax){
			case NONE:
				addFragmentShaderSourceIdentifier("getParallax", RShaders.INSTANCE.parallaxNone().getText()); break;
			case PARALLAX_MAPPING:
				addFragmentShaderSourceIdentifier("getParallax", RShaders.INSTANCE.parallaxMapping().getText()); break;
			case PARALLAX_OCCLUSION_MAPPING:
				addFragmentShaderSourceIdentifier("getParallax", RShaders.INSTANCE.parallaxOcclusionMapping().getText()); break;
			}
		}else{
			addVertexShaderSourceIdentifier("setTangentVectors", RShaders.INSTANCE.varyingTangentsNoneVert().getText());
			addFragmentShaderSourceIdentifier("getBumpedNormal", RShaders.INSTANCE.bumpedNormalNoneFrag().getText());
			
			addFragmentShaderSourceIdentifier("getParallax", RShaders.INSTANCE.parallaxNone().getText());
		}
		
		finishAttaching(gl);
	}
	
	protected void attachShadowShaders(WebGLRenderingContext gl){
		addFragmentShaderSourceIdentifier("isFragShadowed", RShaders.INSTANCE.isShadowedNever().getText());
	}

	@Override
	protected void setVertexAttributes(WebGLRenderingContext gl) {
		
		addVertexAttribute(gl, "vertexPosition", VERTEX_POSITION_ATTRIBUTE);
		addVertexAttribute(gl, "vertexTex", VERTEX_TEX_ATTRIBUTE);
		addVertexAttribute(gl, "vertexNormal", VERTEX_NORMAL_ATTRIBUTE);
		if(normalMapping){ 
			addVertexAttribute(gl, "vertexTangentS", VERTEX_TANGENT_S_ATTRIBUTE);
			addVertexAttribute(gl, "vertexTangentT", VERTEX_TANGENT_T_ATTRIBUTE);
		}
	}

	@Override
	protected void setUniforms(WebGLRenderingContext gl) {
	    super.setUniforms(gl);
		
	    textureColorUniform = gl.getUniformLocation(shaderProgram, "texColor");
	    if(normalMapping){ 
	    	textureBumpUniform = gl.getUniformLocation(shaderProgram, "texBump"); 
	    	reliefMappingHeightUniform = gl.getUniformLocation(shaderProgram, "reliefMappingHeight");
	    }
	    
	    //material
	    diffuseCoefficientUniform = gl.getUniformLocation(shaderProgram, "mtlDiffuseCoefficient");
	    specularCoefficientUniform = gl.getUniformLocation(shaderProgram, "mtlSpecularCoefficient");
	    specularExponentUniform = gl.getUniformLocation(shaderProgram, "mtlSpecularExponent");
	    
	    //light uniforms
		lightPositionUniform = gl.getUniformLocation(shaderProgram, "lightPosition");
	    lightColorUniform = gl.getUniformLocation(shaderProgram, "lightColor");
	    lightAttenuationUniform = gl.getUniformLocation(shaderProgram, "lightAttenuation");
	    
	    fogDensityUniform = gl.getUniformLocation(shaderProgram, "fogDensity");
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(WebGLRenderingContext gl){
		super.useShader(gl);

		//set texture
		gl.uniform1i(textureColorUniform, COLOR_TEXTURE_UNIT);
		if(normalMapping){
			gl.uniform1i(textureBumpUniform, BUMP_TEXTURE_UNIT);
		}
	}
	
	public void setLightAndFog(WebGLRenderingContext gl, Light light, float fogDensity){
		
		//set light position and color, fog density
		Vector3d lightPosition = light.getCamera().getPosition();
		gl.uniform3f(lightPositionUniform, lightPosition.getX(), lightPosition.getY(), lightPosition.getZ());
		
		Vector3d lightColor = light.getColor();
		gl.uniform3f(lightColorUniform, lightColor.getX(), lightColor.getY(), lightColor.getZ());
		
		Vector3d lightAttenuation = light.getAttenuation();
		gl.uniform3f(lightAttenuationUniform, lightAttenuation.getX(), lightAttenuation.getY(), lightAttenuation.getZ());
		
		gl.uniform1f(fogDensityUniform, fogDensity);
	}
	
	public void setMaterialLightingCoefficients(WebGLRenderingContext gl, Material<?> material){
		
		//set material lighting coefficient uniforms
		gl.uniform1f(diffuseCoefficientUniform, material.getDiffuseCoefficient());
		gl.uniform1f(specularCoefficientUniform, material.getSpecularCoefficient());
		gl.uniform1f(specularExponentUniform, material.getSpecularExponent());
	}
	
	public void setReliefMappingHeight(WebGLRenderingContext gl, float reliefMappingHeight){
		
		//set relief mapping height
		gl.uniform1f(reliefMappingHeightUniform, reliefMappingHeight);
	}
	
	public void setVertexAttributeBuffers(WebGLRenderingContext gl, Drawable<?,WGLBuffer> drawable){
		
		//bind vertex data
		drawable.getPositionBuffer().bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
		drawable.getTexBuffer().bindAsVertexAttrib(gl, VERTEX_TEX_ATTRIBUTE);
		drawable.getNormalBuffer().bindAsVertexAttrib(gl, VERTEX_NORMAL_ATTRIBUTE);
		if(normalMapping){
			drawable.getTangentSBuffer().bindAsVertexAttrib(gl, VERTEX_TANGENT_S_ATTRIBUTE);
			drawable.getTangentTBuffer().bindAsVertexAttrib(gl, VERTEX_TANGENT_T_ATTRIBUTE);
		}
	}
}
