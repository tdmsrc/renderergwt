package engine.renderer.gwt.shader.lightpass;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLUniformLocation;

import engine.renderer.Drawable;
import engine.renderer.gwt.object.WGLBuffer;
import engine.renderer.gwt.shader.RShaders;
import geometry.math.Vector3d;


public class ShaderWireframe extends ShaderGeneric{

	public static final int 
		VERTEX_POSITION_ATTRIBUTE = 0;
	
	//wireframe color
	private WebGLUniformLocation colorUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderWireframe(WebGLRenderingContext gl){
		super(gl, "Wireframe shader");
		
		setPrimaryVertexShaderSource(RShaders.INSTANCE.shaderWireframeVert().getText());
		setPrimaryFragmentShaderSource(RShaders.INSTANCE.shaderWireframeFrag().getText());
		
		finishAttaching(gl);
	}

	@Override
	protected void setVertexAttributes(WebGLRenderingContext gl) {
		
		addVertexAttribute(gl, "vertexPosition", VERTEX_POSITION_ATTRIBUTE);
	}

	@Override
	protected void setUniforms(WebGLRenderingContext gl) {
		super.setUniforms(gl);
		
	    colorUniform = gl.getUniformLocation(shaderProgram, "wireframeColor");
	}

	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================

	public void setColor(WebGLRenderingContext gl, Vector3d color){
		
		//set color uniform
		gl.uniform3f(colorUniform, color.getX(), color.getY(), color.getZ());
	}
	
	public void setVertexAttributeBuffers(WebGLRenderingContext gl, Drawable<?,WGLBuffer> drawable){

		drawable.getWireframeBuffer().bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
	}
}
