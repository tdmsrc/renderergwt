package engine.renderer.gwt.shader.lightpass;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLUniformLocation;

import engine.renderer.Drawable;
import engine.renderer.gwt.object.WGLBuffer;
import engine.renderer.gwt.shader.RShaders;
import engine.scene.DrawOptions;
import geometry.math.Vector3d;

//similar to ShaderLightPass,
//but with ambient light data instead of light, and additional emissive light data

public class ShaderAmbientAndEmissive extends ShaderGeneric{
	
	public static final int COLOR_TEXTURE_UNIT = 1;
	public static final int BUMP_TEXTURE_UNIT = 2; //[TODO] only used if normalMapping == true
	
	public static final int 
		VERTEX_POSITION_ATTRIBUTE = 0,
		VERTEX_TEX_ATTRIBUTE = 1,
		VERTEX_NORMAL_ATTRIBUTE = 2,
		VERTEX_TANGENT_S_ATTRIBUTE = 3, //[TODO] only used if normalMapping == true
		VERTEX_TANGENT_T_ATTRIBUTE = 4; //[TODO] only used if normalMapping == true
	
	//options for attaching shaders
	private static final OptionParallax optionParallax = OptionParallax.PARALLAX_OCCLUSION_MAPPING;
	
	//texture/material data
	private WebGLUniformLocation textureColorUniform;
	private WebGLUniformLocation textureBumpUniform; //[TODO] only used if normalMapping == true
	private WebGLUniformLocation reliefMappingHeightUniform; //[TODO] only used if normalMapping == true
	//light options
	private WebGLUniformLocation ambientCoefficientUniform, ambientColorUniform;
	private WebGLUniformLocation emissiveCoefficientUniform, emissiveColorUniform;
	private WebGLUniformLocation fogDensityUniform;
	
	//shader options
	private boolean normalMapping;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderAmbientAndEmissive(WebGLRenderingContext gl, boolean normalMapping){
		super(gl, "Ambient and emissive shader [normalMapping: " + normalMapping + "]");
		
		this.normalMapping = normalMapping;
		
		addVertexShaderSourceIdentifier("getFogIntensity", RShaders.INSTANCE.fogIntensity().getText());
		
		setPrimaryVertexShaderSource(RShaders.INSTANCE.shaderAmbientAndEmissiveVert().getText());
		setPrimaryFragmentShaderSource(RShaders.INSTANCE.shaderAmbientAndEmissiveFrag().getText());
		
		if(normalMapping){
			
			addVertexShaderSourceIdentifier("setTangentVectors", RShaders.INSTANCE.varyingTangentsTextureVert().getText());
			switch(optionParallax){
			case NONE:
				addFragmentShaderSourceIdentifier("getParallax", RShaders.INSTANCE.parallaxNone().getText()); break;
			case PARALLAX_MAPPING:
				addFragmentShaderSourceIdentifier("getParallax", RShaders.INSTANCE.parallaxMapping().getText()); break;
			case PARALLAX_OCCLUSION_MAPPING:
				addFragmentShaderSourceIdentifier("getParallax", RShaders.INSTANCE.parallaxOcclusionMapping().getText()); break;
			}
		}else{
			addVertexShaderSourceIdentifier("setTangentVectors", RShaders.INSTANCE.varyingTangentsNoneVert().getText());
			addFragmentShaderSourceIdentifier("getParallax", RShaders.INSTANCE.parallaxNone().getText());
		}
		
		finishAttaching(gl);
	}

	@Override
	protected void setVertexAttributes(WebGLRenderingContext gl) {
		
		addVertexAttribute(gl, "vertexPosition", VERTEX_POSITION_ATTRIBUTE);
		addVertexAttribute(gl, "vertexTex", VERTEX_TEX_ATTRIBUTE);
		addVertexAttribute(gl, "vertexNormal", VERTEX_NORMAL_ATTRIBUTE);
		if(normalMapping){
			addVertexAttribute(gl, "vertexTangentS", VERTEX_TANGENT_S_ATTRIBUTE);
			addVertexAttribute(gl, "vertexTangentT", VERTEX_TANGENT_T_ATTRIBUTE);
		}
	}

	@Override
	protected void setUniforms(WebGLRenderingContext gl) {
	    super.setUniforms(gl);
		
	    textureColorUniform = gl.getUniformLocation(shaderProgram, "texColor");
	    if(normalMapping){
	    	textureBumpUniform = gl.getUniformLocation(shaderProgram, "texBump"); 
	    	reliefMappingHeightUniform = gl.getUniformLocation(shaderProgram, "reliefMappingHeight");
	    }

	    //light uniforms
	    ambientCoefficientUniform = gl.getUniformLocation(shaderProgram, "ambientCoefficient");
	    ambientColorUniform = gl.getUniformLocation(shaderProgram, "ambientColor");
	    emissiveCoefficientUniform = gl.getUniformLocation(shaderProgram, "emissiveCoefficient");
	    emissiveColorUniform = gl.getUniformLocation(shaderProgram, "emissiveColor");
	    
	    fogDensityUniform = gl.getUniformLocation(shaderProgram, "fogDensity");
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(WebGLRenderingContext gl){
		super.useShader(gl);

		//set texture
		gl.uniform1i(textureColorUniform, COLOR_TEXTURE_UNIT);
		if(normalMapping){
			gl.uniform1i(textureBumpUniform, BUMP_TEXTURE_UNIT);
		}
	}
	
	public void setFog(WebGLRenderingContext gl, float fogDensity){
		
		//set fog density
		gl.uniform1f(fogDensityUniform, fogDensity);
	}
	
	public void setAmbient(WebGLRenderingContext gl, Vector3d ambientColor, float ambientCoefficient){
		
		//set material lighting coefficient uniforms
		gl.uniform3f(ambientColorUniform, ambientColor.getX(), ambientColor.getY(), ambientColor.getZ());
		gl.uniform1f(ambientCoefficientUniform, ambientCoefficient);
	}
	
	public void setEmissive(WebGLRenderingContext gl, DrawOptions drawOptions){
		
		if(drawOptions.checkFlags(DrawOptions.MASK_EMISSIVE, DrawOptions.MASK_EMISSIVE)){
			Vector3d emissiveColor = drawOptions.getEmissiveColor();
			gl.uniform3f(emissiveColorUniform, emissiveColor.getX(), emissiveColor.getY(), emissiveColor.getZ());
			gl.uniform1f(emissiveCoefficientUniform, drawOptions.getEmissiveCoefficient());
		}else{
			gl.uniform3f(emissiveColorUniform, ZERO_VECTOR.getX(), ZERO_VECTOR.getY(), ZERO_VECTOR.getZ());
			gl.uniform1f(emissiveCoefficientUniform, 0.0f);
		}
	}
	
	public void setReliefMappingHeight(WebGLRenderingContext gl, float reliefMappingHeight){
		
		//set relief mapping height
		gl.uniform1f(reliefMappingHeightUniform, reliefMappingHeight);
	}
	
	public void setVertexAttributeBuffers(WebGLRenderingContext gl, Drawable<?,WGLBuffer> drawable){
		
		//bind vertex data
		drawable.getPositionBuffer().bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
		drawable.getTexBuffer().bindAsVertexAttrib(gl, VERTEX_TEX_ATTRIBUTE);
		drawable.getNormalBuffer().bindAsVertexAttrib(gl, VERTEX_NORMAL_ATTRIBUTE);
		if(normalMapping){
			drawable.getTangentSBuffer().bindAsVertexAttrib(gl, VERTEX_TANGENT_S_ATTRIBUTE);
			drawable.getTangentTBuffer().bindAsVertexAttrib(gl, VERTEX_TANGENT_T_ATTRIBUTE);
		}
	}
}
