package engine.renderer.gwt.shader.lightpass;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLUniformLocation;

import engine.renderer.gwt.shader.RShaders;
import engine.scene.Light;


public class ShaderLightPassDirectional extends ShaderLightPass{

	public static final int SHADOW_MAP_TEXTURE_UNIT = 0;
	
	private WebGLUniformLocation textureShadowMapUniform;
	
	private float[] lightProjectionMatrixArray, lightModelViewRotationMatrixArray;
	private WebGLUniformLocation lightProjectionMatrixUniform, lightModelViewRotationMatrixUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderLightPassDirectional(WebGLRenderingContext gl, boolean normalMapping) {
		super(gl, normalMapping);
		
		lightProjectionMatrixArray = new float[16];
		lightModelViewRotationMatrixArray = new float[9];
	}
	
	@Override
	protected void attachShadowShaders(WebGLRenderingContext gl){
		addFragmentShaderSourceIdentifier("isFragShadowed", RShaders.INSTANCE.isShadowedDirectional().getText());
	}

	@Override
	protected void setUniforms(WebGLRenderingContext gl) {
		super.setUniforms(gl);
		
		textureShadowMapUniform = gl.getUniformLocation(shaderProgram, "texShadowMap");
		
		lightProjectionMatrixUniform = gl.getUniformLocation(shaderProgram, "lightProjectionMatrix");
		lightModelViewRotationMatrixUniform = gl.getUniformLocation(shaderProgram, "lightModelViewRotation");
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(WebGLRenderingContext gl){
		super.useShader(gl);
		
		gl.uniform1i(textureShadowMapUniform, SHADOW_MAP_TEXTURE_UNIT);
	}
	
	public void setAmbientToShadowMapTransformation(WebGLRenderingContext gl, Light light){
		
		light.getCamera().getRotation().getMatrix().serializeColumnMajor(lightModelViewRotationMatrixArray);
		gl.uniformMatrix3fv(lightModelViewRotationMatrixUniform, false, lightModelViewRotationMatrixArray);
		
		light.getCamera().getProjectionMatrix().serializeColumnMajor(lightProjectionMatrixArray);
		gl.uniformMatrix4fv(lightProjectionMatrixUniform, false, lightProjectionMatrixArray);
	}
}
