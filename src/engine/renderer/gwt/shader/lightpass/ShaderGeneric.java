package engine.renderer.gwt.shader.lightpass;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLUniformLocation;

import engine.renderer.gwt.shader.RShaders;
import engine.renderer.gwt.shader.ShaderProgram;
import engine.scene.SceneObject;
import geometry.math.Camera;
import geometry.math.Matrix3d;
import geometry.math.Vector3d;

public abstract class ShaderGeneric extends ShaderProgram{
	
	protected static Matrix3d IDENTITY_MATRIX = Matrix3d.createIdentityMatrix();
	protected static Vector3d ZERO_VECTOR = new Vector3d();
	
	//options
	public static enum OptionParallax{
		NONE, PARALLAX_MAPPING, PARALLAX_OCCLUSION_MAPPING
	}
	
	public static enum OptionShadowFilter{
		NONE, PERCENTAGE_CLOSER_FILTERING
	}
	
	//transformations
	private float[] projectionMatrixArray, modelViewRotationMatrixArray;
	private WebGLUniformLocation projectionMatrixUniform, modelViewRotationMatrixUniform, eyePositionUniform;
	
	private float[] objRotationMatrixArray;
	private WebGLUniformLocation objRotationMatrixUniform, objTranslationUniform, objOffsetUniform;
	
	//identity matrix entries in column-major order, used when object is not transformed
	private float[] identityMatrixArray; 
	
	//for billboarding
	private float[] modelViewRotationInverseMatrixArray;
	private WebGLUniformLocation useBillboardUniform, modelViewRotationInverseMatrixUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderGeneric(WebGLRenderingContext gl, String name){
		super(gl, name);
		//addVertexShaderSourceIdentifier("getObjRotation", RShaders.INSTANCE.objRotation().getText());
		addVertexShaderSourceIdentifier("getObjRotation", RShaders.INSTANCE.objRotationBillboard().getText());
		
		projectionMatrixArray = new float[16];
		modelViewRotationMatrixArray = new float[9];
		objRotationMatrixArray = new float[9];
		
		modelViewRotationInverseMatrixArray = new float[9];
		
		identityMatrixArray = new float[9];
		IDENTITY_MATRIX.serializeColumnMajor(identityMatrixArray);
	}
	
	@Override
	protected void setUniforms(WebGLRenderingContext gl) {
		
		//get uniform locations
	    projectionMatrixUniform = gl.getUniformLocation(shaderProgram, "projectionMatrix");
	    modelViewRotationMatrixUniform = gl.getUniformLocation(shaderProgram, "modelViewRotation");
	    eyePositionUniform = gl.getUniformLocation(shaderProgram, "eyePosition");
	    
	    objRotationMatrixUniform = gl.getUniformLocation(shaderProgram, "objRotation");
	    objTranslationUniform = gl.getUniformLocation(shaderProgram, "objTranslation");
	    objOffsetUniform = gl.getUniformLocation(shaderProgram, "objOffset");
	    
	    useBillboardUniform = gl.getUniformLocation(shaderProgram, "useBillboard");
	    modelViewRotationInverseMatrixUniform = gl.getUniformLocation(shaderProgram, "modelViewRotationInverse");
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	public void setViewMatrices(WebGLRenderingContext gl, Camera camera){
		
		camera.getProjectionMatrix().serializeColumnMajor(projectionMatrixArray);
		gl.uniformMatrix4fv(projectionMatrixUniform, false, projectionMatrixArray);
		
		camera.getRotation().getMatrix().serializeColumnMajor(modelViewRotationMatrixArray);
		gl.uniformMatrix3fv(modelViewRotationMatrixUniform, false, modelViewRotationMatrixArray);
		
		Vector3d eyePosition = camera.getPosition();
		gl.uniform3f(eyePositionUniform, eyePosition.getX(), eyePosition.getY(), eyePosition.getZ());
		
		//could actually use glUniformMatrix3fv(modelviewRotationMatrix) with "transpose" set to true
		camera.getRotation().getMatrixInverse().serializeColumnMajor(modelViewRotationInverseMatrixArray);
		gl.uniformMatrix3fv(modelViewRotationInverseMatrixUniform, false, modelViewRotationInverseMatrixArray);
	}
	
	public void setObjectTransformation(WebGLRenderingContext gl, SceneObject<?,?,?> object){

		if(object.isTransformed()){
			//pull rotation, translation, and offset from object transformation
			object.getObjectTransformation().getRotation().getMatrix().serializeColumnMajor(objRotationMatrixArray);
			gl.uniformMatrix3fv(objRotationMatrixUniform, false, objRotationMatrixArray);
			
			Vector3d objTrans = object.getObjectTransformation().getTranslation();
			gl.uniform3f(objTranslationUniform, objTrans.getX(), objTrans.getY(), objTrans.getZ());
			
			Vector3d objOffset = object.getObjectTransformation().getOffset();
			gl.uniform3f(objOffsetUniform, objOffset.getX(), objOffset.getY(), objOffset.getZ());
			
		}else{
			//use identity matrix and zero vectors
			gl.uniformMatrix3fv(objRotationMatrixUniform, false, identityMatrixArray);
			gl.uniform3f(objTranslationUniform, ZERO_VECTOR.getX(), ZERO_VECTOR.getY(), ZERO_VECTOR.getZ());
			gl.uniform3f(objOffsetUniform, ZERO_VECTOR.getX(), ZERO_VECTOR.getY(), ZERO_VECTOR.getZ());
		}
		
		gl.uniform1i(useBillboardUniform, object.isBillboard() ? 1 : 0);
	}
}
