package engine.renderer.gwt.shader;

import java.util.ArrayList;
import java.util.HashMap;

import com.googlecode.gwtgl.binding.WebGLProgram;
import com.googlecode.gwtgl.binding.WebGLRenderingContext;

import engine.renderer.shadertools.GLSLESPreprocessor;
import geometry.common.MessageOutput;


public abstract class ShaderProgram{
	private static final boolean CHECK_ERRORS = true;
	
	private String name; //just used for outputting debug info/errors
	
	protected WebGLProgram shaderProgram;
	private ArrayList<Integer> vertexAttributes;
	
	//specified sources for preprocessing [only needed until finishAttaching is called]
	private String primaryVertexShaderSource, primaryFragmentShaderSource;
	private HashMap<String,String> vertexShaderSourceIdentifiers, fragmentShaderSourceIdentifiers;
	
	
	public ShaderProgram(WebGLRenderingContext gl, String name){ 
		this.name = name;
		
		vertexAttributes = new ArrayList<Integer>();
		shaderProgram = gl.createProgram();
		
		vertexShaderSourceIdentifiers = new HashMap<String,String>();
		fragmentShaderSourceIdentifiers = new HashMap<String,String>();
	}
	
	
	protected void setPrimaryVertexShaderSource(String source){
		primaryVertexShaderSource = source;
	}
	
	protected void setPrimaryFragmentShaderSource(String source){
		primaryFragmentShaderSource = source;
	}
	
	protected void addVertexShaderSourceIdentifier(String identifier, String source){
		vertexShaderSourceIdentifiers.put(identifier, source);
	}
	
	protected void addFragmentShaderSourceIdentifier(String identifier, String source){
		fragmentShaderSourceIdentifiers.put(identifier, source);
	}
	
	
	protected void finishAttaching(WebGLRenderingContext gl){
		
		//do preprocessing to link multiple sources (necessary because of GLES 2.0); attach resulting source
		String vertexShaderSource = GLSLESPreprocessor.process(primaryVertexShaderSource, vertexShaderSourceIdentifiers);
		VertexShader vs = new VertexShader(gl, vertexShaderSource);
		gl.attachShader(shaderProgram, vs.getWebGLShader());
		
		String fragmentShaderSource = GLSLESPreprocessor.process(primaryFragmentShaderSource, fragmentShaderSourceIdentifiers);
		FragmentShader fs = new FragmentShader(gl, fragmentShaderSource);
		gl.attachShader(shaderProgram, fs.getWebGLShader());
		
		//bind vertex attributes
		setVertexAttributes(gl);
		
		//link the shader program
		MessageOutput.printDebug("Linking program (" + name + ")");
		gl.linkProgram(shaderProgram);
		if(CHECK_ERRORS){ checkLinkStatus(gl); }

		//set up uniforms that will be used
		setUniforms(gl);
	}

	
	protected abstract void setVertexAttributes(WebGLRenderingContext gl);
	
	protected void addVertexAttribute(WebGLRenderingContext gl, String name, int index){
		vertexAttributes.add(index);
		gl.bindAttribLocation(shaderProgram, index, name);
	}
	
	protected abstract void setUniforms(WebGLRenderingContext gl);

	public void useShader(WebGLRenderingContext gl){
		
		//enable shader
		gl.useProgram(shaderProgram);
		//enable attributes
		for(int index : vertexAttributes){ gl.enableVertexAttribArray(index); }
	}
	
	public void unuseShader(WebGLRenderingContext gl){
		
		//disable attributes
		for(int index : vertexAttributes){ gl.disableVertexAttribArray(index); }
		//disable shader
		gl.useProgram(null);
	}
	
    
    public void checkLinkStatus(WebGLRenderingContext gl){
		
        boolean status = gl.getProgramParameterb(shaderProgram, WebGLRenderingContext.LINK_STATUS);

        MessageOutput.printDebug("Checking GL_LINK_STATUS (" + name + ")");
        if(status == false){
        	MessageOutput.printDebug("GL_LINK_STATUS is GL_FALSE! Info log: ");
        	throw new Error(getProgramInfoLog(gl));
        }else{
        	MessageOutput.printDebug("GL_LINK_STATUS is ok. Info log: \n" + getProgramInfoLog(gl));
        }
    }

    public void checkValidateStatus(WebGLRenderingContext gl){
		
		boolean status = gl.getProgramParameterb(shaderProgram, WebGLRenderingContext.VALIDATE_STATUS);
		
		MessageOutput.printDebug("Checking GL_VALIDATE_STATUS (" + name + ")");
		if(status == false){
			MessageOutput.printDebug("GL_VALIDATE_STATUS is GL_FALSE! Info log: ");
			throw new Error(getProgramInfoLog(gl));
		}else{
			MessageOutput.printDebug("GL_VALIDATE_STATUS is ok. Info log: \n" + getProgramInfoLog(gl));
		}
	}
    
    public String getProgramInfoLog(WebGLRenderingContext gl){
   	 
        String infoLog = gl.getProgramInfoLog(shaderProgram);

        return infoLog;
    }
}
