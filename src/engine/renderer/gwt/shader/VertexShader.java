package engine.renderer.gwt.shader;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;

import geometry.common.MessageOutput;


public class VertexShader extends Shader{

	public VertexShader(WebGLRenderingContext gl, String vertexShaderSource){
		super(gl.createShader(WebGLRenderingContext.VERTEX_SHADER));
		
		MessageOutput.printDebug("Compiling vertex shader");
		gl.shaderSource(shader, vertexShaderSource);
		gl.compileShader(shader);
		if(CHECK_ERRORS){ checkCompileStatus(gl); }
	}
}