package engine.renderer.gwt.shader.skybox;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLUniformLocation;

import engine.renderer.gwt.object.WGLBuffer;
import engine.renderer.gwt.shader.RShaders;
import engine.renderer.gwt.shader.ShaderProgram;
import engine.scene.Skybox;
import engine.scene.Skybox.SkyboxFace;
import geometry.math.Camera;


public class ShaderSkybox extends ShaderProgram{

	public static final int SKYBOX_TEXTURE_UNIT = 0;
	public static final int 
		VERTEX_POSITION_ATTRIBUTE = 0,
		VERTEX_TEX_ATTRIBUTE = 1;
	
	//texture
	private WebGLUniformLocation textureColorUniform;
	//transformations
	private float[] projectionMatrixArray, modelViewRotationMatrixArray;
	private WebGLUniformLocation projectionMatrixUniform, modelViewRotationMatrixUniform;
	

	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderSkybox(WebGLRenderingContext gl){
		super(gl, "Skybox shader");
		setPrimaryVertexShaderSource(RShaders.INSTANCE.shaderSkyboxVert().getText());
		setPrimaryFragmentShaderSource(RShaders.INSTANCE.shaderSkyboxFrag().getText());
		finishAttaching(gl);
		
		projectionMatrixArray = new float[16];
		modelViewRotationMatrixArray = new float[9];
	}

	@Override
	protected void setVertexAttributes(WebGLRenderingContext gl){

		addVertexAttribute(gl, "vertexPosition", VERTEX_POSITION_ATTRIBUTE);
		addVertexAttribute(gl, "vertexTex", VERTEX_TEX_ATTRIBUTE);
	}

	@Override
	protected void setUniforms(WebGLRenderingContext gl){

		//get uniform locations
		textureColorUniform = gl.getUniformLocation(shaderProgram, "texColor");
		
	    projectionMatrixUniform = gl.getUniformLocation(shaderProgram, "projectionMatrix");
	    modelViewRotationMatrixUniform = gl.getUniformLocation(shaderProgram, "modelViewRotation"); 
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(WebGLRenderingContext gl){
		super.useShader(gl);

		//set textures
		gl.uniform1i(textureColorUniform, SKYBOX_TEXTURE_UNIT);
	}
	
	public void setViewMatrices(WebGLRenderingContext gl, Camera camera){
		
		camera.getProjectionMatrix().serializeColumnMajor(projectionMatrixArray);
		gl.uniformMatrix4fv(projectionMatrixUniform, false, projectionMatrixArray);
		
		camera.getRotation().getMatrix().serializeColumnMajor(modelViewRotationMatrixArray);
		gl.uniformMatrix3fv(modelViewRotationMatrixUniform, false, modelViewRotationMatrixArray);
	}
	
	public void setVertexAttributeBuffers(WebGLRenderingContext gl, Skybox<?,WGLBuffer,?> skybox, SkyboxFace face){

		//bind vertex data
		skybox.getPositionBuffer(face).bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
		skybox.getTexBuffer().bindAsVertexAttrib(gl, VERTEX_TEX_ATTRIBUTE);
	}
	
	public void setVertexAttributeBuffersSun(WebGLRenderingContext gl, Skybox<?,WGLBuffer,?> skybox){
		
		//bind vertex data
		skybox.getSunPositionBuffer().bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
		skybox.getTexBuffer().bindAsVertexAttrib(gl, VERTEX_TEX_ATTRIBUTE);
	}
}
