package engine.renderer.gwt.shader;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;

import geometry.common.MessageOutput;


public class FragmentShader extends Shader{

	public FragmentShader(WebGLRenderingContext gl, String fragmentShaderSource){
		super(gl.createShader(WebGLRenderingContext.FRAGMENT_SHADER));
		
		MessageOutput.printDebug("Compiling fragment shader");
		//MessageOutput.printDebug(fragmentShaderSource);
		
		gl.shaderSource(shader, fragmentShaderSource);
		gl.compileShader(shader);
		if(CHECK_ERRORS){ 
			checkCompileStatus(gl);
		}
	}
}