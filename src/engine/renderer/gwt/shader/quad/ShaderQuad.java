package engine.renderer.gwt.shader.quad;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLUniformLocation;

import engine.renderer.gwt.object.WGLBuffer;
import engine.renderer.gwt.shader.ShaderProgram;
import engine.renderer.gwt.shader.RShaders;
import engine.tools.Quad;


public class ShaderQuad extends ShaderProgram{

	public static final int COLOR_TEXTURE_UNIT = 0;
	
	public static final int 
		VERTEX_POSITION_ATTRIBUTE = 0,
		VERTEX_TEX_ATTRIBUTE = 1;

	//texture data
	private WebGLUniformLocation textureColor;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderQuad(WebGLRenderingContext gl, String name){
		super(gl, name);
		
		setPrimaryVertexShaderSource(RShaders.INSTANCE.shaderQuadVert().getText());
		setPrimaryFragmentShaderSource(RShaders.INSTANCE.shaderQuadFragCopy().getText());
		finishAttaching(gl);
	}
	
	public ShaderQuad(WebGLRenderingContext gl, String name, String streamFragmentShaderSource){
		super(gl, name);
		setPrimaryVertexShaderSource(RShaders.INSTANCE.shaderQuadVert().getText());
		setPrimaryFragmentShaderSource(streamFragmentShaderSource);
		finishAttaching(gl);
	}
	
	@Override
	protected void setVertexAttributes(WebGLRenderingContext gl) {

		addVertexAttribute(gl, "vertexPosition", VERTEX_POSITION_ATTRIBUTE);
		addVertexAttribute(gl, "vertexTex", VERTEX_TEX_ATTRIBUTE);
	}
	
	@Override
	protected void setUniforms(WebGLRenderingContext gl) {
  
		//get uniform locations
	    textureColor = gl.getUniformLocation(shaderProgram, "texColor");
	}
	
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(WebGLRenderingContext gl){
		super.useShader(gl);

		//set texture
		gl.uniform1i(textureColor, COLOR_TEXTURE_UNIT);
	}
	
	public void setVertexAttributeBuffers(WebGLRenderingContext gl, Quad<?,WGLBuffer> quad){

		//bind vertex data
		quad.getPositionBuffer().bindAsVertexAttrib(gl, VERTEX_POSITION_ATTRIBUTE);
		quad.getTexBuffer().bindAsVertexAttrib(gl, VERTEX_TEX_ATTRIBUTE);
	}
}
