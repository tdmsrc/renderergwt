package engine.renderer.gwt.shader.quad;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLUniformLocation;

import engine.renderer.gwt.shader.RShaders;

public class ShaderQuadHSVAdjust extends ShaderQuad{

	//adjustment parameters
	private WebGLUniformLocation hsvAdjustHueUniform;
	private WebGLUniformLocation hsvAdjustSaturationUniform;
	private WebGLUniformLocation hsvAdjustValueUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderQuadHSVAdjust(WebGLRenderingContext gl){
		super(gl, "HSV adjust quad shader", RShaders.INSTANCE.shaderQuadFragHSVAdjust().getText());
		
	}

	@Override
	protected void setUniforms(WebGLRenderingContext gl) {
		super.setUniforms(gl);
		
		//get uniform locations
		hsvAdjustHueUniform = gl.getUniformLocation(shaderProgram, "hsvAdjustHue");
		hsvAdjustSaturationUniform = gl.getUniformLocation(shaderProgram, "hsvAdjustSaturation");
		hsvAdjustValueUniform = gl.getUniformLocation(shaderProgram, "hsvAdjustValue");
		
	}
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	public void setHSVAdjust(WebGLRenderingContext gl, float hsvAdjustHue, float hsvAdjustSaturation, float hsvAdjustValue){
		
		gl.uniform1f(hsvAdjustHueUniform, hsvAdjustHue);
		gl.uniform1f(hsvAdjustSaturationUniform, hsvAdjustSaturation);
		gl.uniform1f(hsvAdjustValueUniform, hsvAdjustValue);
		
	}
}
