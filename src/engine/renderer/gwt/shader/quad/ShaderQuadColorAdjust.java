package engine.renderer.gwt.shader.quad;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLUniformLocation;

import engine.renderer.gwt.shader.RShaders;

public class ShaderQuadColorAdjust extends ShaderQuad{

	//adjustment parameters
	private WebGLUniformLocation colorAdjustBrightnessUniform;
	private WebGLUniformLocation colorAdjustContrastUniform;
	private WebGLUniformLocation colorAdjustSaturationUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderQuadColorAdjust(WebGLRenderingContext gl){
		super(gl, "Color adjust quad shader", RShaders.INSTANCE.shaderQuadFragColorAdjust().getText());
		
	}

	@Override
	protected void setUniforms(WebGLRenderingContext gl) {
		super.setUniforms(gl);
		
		//get uniform locations
		colorAdjustBrightnessUniform = gl.getUniformLocation(shaderProgram, "colorAdjustBrightness");
		colorAdjustContrastUniform = gl.getUniformLocation(shaderProgram, "colorAdjustContrast");
		colorAdjustSaturationUniform = gl.getUniformLocation(shaderProgram, "colorAdjustSaturation");
	}
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	public void setColorAdjust(WebGLRenderingContext gl, float colorAdjustBrightness, float colorAdjustContrast, float colorAdjustSaturation){
		
		gl.uniform1f(colorAdjustBrightnessUniform, colorAdjustBrightness);
		gl.uniform1f(colorAdjustContrastUniform, colorAdjustContrast);
		gl.uniform1f(colorAdjustSaturationUniform, colorAdjustSaturation);
	}
}
