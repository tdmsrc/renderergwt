package engine.renderer.gwt.shader.quad;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLUniformLocation;

import engine.renderer.gwt.shader.RShaders;

public class ShaderQuadBlur extends ShaderQuad{

	//texture data
	private WebGLUniformLocation textureSizeUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public enum BlurDirection{ BLUR_DIRECTION_HORIZONTAL, BLUR_DIRECTION_VERTICAL; }
	
	public ShaderQuadBlur(WebGLRenderingContext gl, BlurDirection blurDirection){
		super(gl, 
			(blurDirection == BlurDirection.BLUR_DIRECTION_HORIZONTAL) ? 
				"BlurH quad shader" : "BlurV quad shader",
			(blurDirection == BlurDirection.BLUR_DIRECTION_HORIZONTAL) ? 
				RShaders.INSTANCE.shaderQuadFragBlurH().getText() :
				RShaders.INSTANCE.shaderQuadFragBlurV().getText()
		);
	}

	@Override
	protected void setUniforms(WebGLRenderingContext gl) {
		super.setUniforms(gl);
		
		//get uniform locations
		textureSizeUniform = gl.getUniformLocation(shaderProgram, "texSize");
	}
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	public void setTextureSize(WebGLRenderingContext gl, int textureWidth, int textureHeight){
		
		gl.uniform2i(textureSizeUniform, textureWidth, textureHeight);
	}
}
