package engine.renderer.gwt.shader.quad;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLUniformLocation;

import engine.renderer.gwt.shader.RShaders;

public class ShaderQuadCopyAlpha extends ShaderQuad{
	
	//adjustment parameters
	private WebGLUniformLocation alphaUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderQuadCopyAlpha(WebGLRenderingContext gl){
		super(gl, "Copy alpha quad shader", RShaders.INSTANCE.shaderQuadFragCopyAlpha().getText());
	}
	
	@Override
	protected void setUniforms(WebGLRenderingContext gl) {
		super.setUniforms(gl);
		
		//get uniform locations
		alphaUniform = gl.getUniformLocation(shaderProgram, "alpha");
	}
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	public void setAlpha(WebGLRenderingContext gl, float alpha){
		
		gl.uniform1f(alphaUniform, alpha);
	}
}
