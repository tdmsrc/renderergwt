package engine.renderer.gwt.shader.quad;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLUniformLocation;

import engine.renderer.gwt.shader.RShaders;

public class ShaderQuadDepthOfField extends ShaderQuad{
	
	public static final int DEPTH_TEXTURE_UNIT = 1;
	public static final int BLUR_TEXTURE_UNIT = 2;

	//depth texture 
	private WebGLUniformLocation depthTextureUniform;
	private WebGLUniformLocation blurTextureUniform;
	
	//adjustment parameters
	private WebGLUniformLocation zNearInvUniform;
	private WebGLUniformLocation zFarInvUniform;
	private WebGLUniformLocation dFocusTargetUniform;
	private WebGLUniformLocation dFocusRangeInvUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderQuadDepthOfField(WebGLRenderingContext gl){
		super(gl, "Depth field quad shader", RShaders.INSTANCE.shaderQuadFragDepthOfField().getText());
		
	}

	@Override
	protected void setUniforms(WebGLRenderingContext gl) {
		super.setUniforms(gl);
		
		//get uniform locations
		depthTextureUniform = gl.getUniformLocation(shaderProgram, "texDepth");
		blurTextureUniform = gl.getUniformLocation(shaderProgram, "texBlur");
		
		zNearInvUniform = gl.getUniformLocation(shaderProgram, "zNearInv");
		zFarInvUniform = gl.getUniformLocation(shaderProgram, "zFarInv");
		dFocusTargetUniform = gl.getUniformLocation(shaderProgram, "dFocusTarget");
		dFocusRangeInvUniform = gl.getUniformLocation(shaderProgram, "dFocusRangeInv");
	}
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	@Override
	public void useShader(WebGLRenderingContext gl){
		super.useShader(gl);
		
		//set textures
		gl.uniform1i(depthTextureUniform, DEPTH_TEXTURE_UNIT);
		gl.uniform1i(blurTextureUniform, BLUR_TEXTURE_UNIT);
	}
	
	public void setParameters(WebGLRenderingContext gl, float zNear, float zFar, float focusTargetDistance, float focusRange){
		
		gl.uniform1f(zNearInvUniform, 1.0f / zNear);
		gl.uniform1f(zFarInvUniform, 1.0f / zFar);
		gl.uniform1f(dFocusTargetUniform, focusTargetDistance);
		gl.uniform1f(dFocusRangeInvUniform, 1.0f / focusRange);
	}
}
