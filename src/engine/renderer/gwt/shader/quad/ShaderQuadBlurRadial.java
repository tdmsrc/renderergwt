package engine.renderer.gwt.shader.quad;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLUniformLocation;

import engine.renderer.gwt.shader.RShaders;
import geometry.math.Vector2d;

public class ShaderQuadBlurRadial extends ShaderQuad{
	
	//adjustment parameters
	private WebGLUniformLocation blurCenterUniform;
	
	
	//===================================
	// INITIALIZATION
	//===================================
	
	public ShaderQuadBlurRadial(WebGLRenderingContext gl){
		super(gl, "Radial blur quad shader", RShaders.INSTANCE.shaderQuadFragBlurRadial().getText());
	}
	
	@Override
	protected void setUniforms(WebGLRenderingContext gl) {
		super.setUniforms(gl);
		
		//get uniform locations
		blurCenterUniform = gl.getUniformLocation(shaderProgram, "blurCenter");
	}
	
	//===================================
	// SETTING BUFFERS AND UNIFORMS
	//===================================
	
	public void setBlurCenter(WebGLRenderingContext gl, Vector2d blurCenter){
		
		gl.uniform2f(blurCenterUniform, blurCenter.getX(), blurCenter.getY());
	}
}
