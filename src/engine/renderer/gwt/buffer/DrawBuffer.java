package engine.renderer.gwt.buffer;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;

import geometry.math.Vector4d;


public interface DrawBuffer{
	
	public int getWidth();
	public int getHeight();

	public void resize(WebGLRenderingContext gl, int width, int height);
	public void delete(WebGLRenderingContext gl);
	
	public void clearDepth(WebGLRenderingContext gl);
	public void clearColor(WebGLRenderingContext gl, Vector4d color);
	public void clearDepthAndColor(WebGLRenderingContext gl, Vector4d color);
	
	public void bindDrawTo(WebGLRenderingContext gl);
	public void unbindDrawTo(WebGLRenderingContext gl);
	
	public void bindAsTexture(WebGLRenderingContext gl, int texUnit);
	public void unbindAsTexture(WebGLRenderingContext gl, int texUnit);
}
