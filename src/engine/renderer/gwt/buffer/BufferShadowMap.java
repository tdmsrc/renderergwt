package engine.renderer.gwt.buffer;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLTexture;

import geometry.common.MessageOutput;


public class BufferShadowMap{

	private int width, height;
	private WebGLTexture texDepth;
	private WebGLTexture texColor;

	
	public BufferShadowMap(WebGLRenderingContext gl, int width, int height){

		this.width = width;
		this.height = height;

		create(gl);
	}

	public int getHeight(){ return height; }
	public int getWidth(){ return width; }
	
	public void resize(WebGLRenderingContext gl, int width, int height){
		
		delete(gl);
		
		this.width = width;
		this.height = height;
		
		create(gl);
	}
	
	private void create(WebGLRenderingContext gl){
		
		//create depth texture
		texDepth = gl.createTexture();
		
		gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, texDepth);
		gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_MIN_FILTER, WebGLRenderingContext.NEAREST);
		gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_MAG_FILTER, WebGLRenderingContext.NEAREST);
		gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_WRAP_S, WebGLRenderingContext.CLAMP_TO_EDGE);
		gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_WRAP_T, WebGLRenderingContext.CLAMP_TO_EDGE);
		gl.texImage2D(WebGLRenderingContext.TEXTURE_2D, 0, WebGLRenderingContext.DEPTH_COMPONENT, width, height, 0, WebGLRenderingContext.DEPTH_COMPONENT, WebGLRenderingContext.UNSIGNED_INT, null);
		
		gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, null);
		
		//check for errors
		switch(gl.getError()){
		case WebGLRenderingContext.INVALID_ENUM: throw new Error("(BufferShadowMap) GL error GL_INVALID_ENUM");
		case WebGLRenderingContext.INVALID_VALUE: throw new Error("(BufferShadowMap) GL error GL_INVALID_VALUE");
		case WebGLRenderingContext.INVALID_OPERATION: throw new Error("(BufferShadowMap) GL error GL_INVALID_OPERATION");
		case WebGLRenderingContext.INVALID_FRAMEBUFFER_OPERATION: throw new Error("(BufferShadowMap) GL error GL_INVALID_FRAMEBUFFER_OPERATION");
		case WebGLRenderingContext.OUT_OF_MEMORY: throw new Error("(BufferShadowMap) GL error GL_OUT_OF_MEMORY");
		}
		
		//create color texture
		texColor = gl.createTexture();
		
		gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, texColor);
		gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_MIN_FILTER, WebGLRenderingContext.LINEAR);
		gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_MAG_FILTER, WebGLRenderingContext.LINEAR);
		gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_WRAP_S, WebGLRenderingContext.CLAMP_TO_EDGE);
		gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_WRAP_T, WebGLRenderingContext.CLAMP_TO_EDGE);
		gl.texImage2D(WebGLRenderingContext.TEXTURE_2D, 0, WebGLRenderingContext.RGBA, width, height, 0, WebGLRenderingContext.RGBA, WebGLRenderingContext.UNSIGNED_BYTE, null);

		gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, null);
		
		//check for errors
		switch(gl.getError()){
		case WebGLRenderingContext.INVALID_ENUM: throw new Error("(BufferColorAndDepth) GL error INVALID_ENUM");
		case WebGLRenderingContext.INVALID_VALUE: throw new Error("(BufferColorAndDepth) GL error INVALID_VALUE");
		case WebGLRenderingContext.INVALID_OPERATION: throw new Error("(BufferColorAndDepth) GL error INVALID_OPERATION");
		case WebGLRenderingContext.INVALID_FRAMEBUFFER_OPERATION: throw new Error("(BufferColorAndDepth) GL error INVALID_FRAMEBUFFER_OPERATION");
		case WebGLRenderingContext.OUT_OF_MEMORY: throw new Error("(BufferColorAndDepth) GL error OUT_OF_MEMORY");
		}
		
		MessageOutput.printDebug("Created shadow map buffer at " + width + "x" + height);
	}
	
	public void delete(WebGLRenderingContext gl){
		gl.deleteTexture(texDepth);
		gl.deleteTexture(texColor);
		
		MessageOutput.printDebug("Deleted directional shadow map");
	}

	public void clearDepth(WebGLRenderingContext gl){

		gl.depthMask(true); 
		gl.clear(WebGLRenderingContext.DEPTH_BUFFER_BIT);
	}
	
	public void bindDrawTo(WebGLRenderingContext gl){
		
		//attach depth texture
		gl.framebufferTexture2D(WebGLRenderingContext.FRAMEBUFFER, 
			WebGLRenderingContext.DEPTH_ATTACHMENT, WebGLRenderingContext.TEXTURE_2D, texDepth, 0);
		
		//attach color texture
		gl.framebufferTexture2D(WebGLRenderingContext.FRAMEBUFFER, 
			WebGLRenderingContext.COLOR_ATTACHMENT0, WebGLRenderingContext.TEXTURE_2D, texColor, 0);
		
		//specify drawbuffers and readbuffers
		//gl.drawBuffer(WebGLRenderingContext.NONE);
		//gl.readBuffer(WebGLRenderingContext.NONE);
		
		//check if it worked
		int status = gl.checkFramebufferStatus(WebGLRenderingContext.FRAMEBUFFER);
		if(status != WebGLRenderingContext.FRAMEBUFFER_COMPLETE){
			switch(status){
			case WebGLRenderingContext.FRAMEBUFFER_INCOMPLETE_ATTACHMENT: 
				throw new Error("(BufferShadowMap) FBO status is GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT."); 
			case WebGLRenderingContext.FRAMEBUFFER_INCOMPLETE_DIMENSIONS: 
				throw new Error("(BufferShadowMap) FBO status is GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS.");
			case WebGLRenderingContext.FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT: 
				throw new Error("(BufferShadowMap) FBO status is GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT.");
			case WebGLRenderingContext.FRAMEBUFFER_UNSUPPORTED: 
				throw new Error("(BufferShadowMap) FBO status is GL_FRAMEBUFFER_UNSUPPORTED.");
			default: 
				throw new Error("(BufferShadowMap) FBO status is not GL_FRAMEBUFFER_COMPLETE.");
			} 
		}
		
		//set appropriate viewport
		gl.viewport(0, 0, width, height);
	}
	
	public void unbindDrawTo(WebGLRenderingContext gl){
		
		//detach texture
		gl.framebufferTexture2D(WebGLRenderingContext.FRAMEBUFFER, 
			WebGLRenderingContext.DEPTH_ATTACHMENT, WebGLRenderingContext.TEXTURE_2D, null, 0);
	}
	
	public void bindAsTexture(WebGLRenderingContext gl, int texUnit){
		
		gl.activeTexture(WebGLRenderingContext.TEXTURE0+texUnit);
	    gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, texDepth);
	}
	
	public void unbindAsTexture(WebGLRenderingContext gl, int texUnit){

		gl.activeTexture(WebGLRenderingContext.TEXTURE0+texUnit);
	    gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, null);
	}
}
