package engine.renderer.gwt.buffer;

import com.googlecode.gwtgl.binding.WebGLFramebuffer;
import com.googlecode.gwtgl.binding.WebGLRenderingContext;

import geometry.common.MessageOutput;


public class FramebufferObject{
	
	WebGLFramebuffer fbo;
	
	public FramebufferObject(WebGLRenderingContext gl){
		
		//create FBO
		fbo = gl.createFramebuffer();
	}
	
	public void delete(WebGLRenderingContext gl){
		
		gl.deleteFramebuffer(fbo);
		
		MessageOutput.printDebug("Deleted FBO");
	}

	public void bind(WebGLRenderingContext gl){
		gl.bindFramebuffer(WebGLRenderingContext.FRAMEBUFFER, fbo);
	}
	
	public void unbind(WebGLRenderingContext gl){
		gl.bindFramebuffer(WebGLRenderingContext.FRAMEBUFFER, null);
	}

}
