package engine.renderer.gwt.buffer;

import com.googlecode.gwtgl.binding.WebGLRenderingContext;
import com.googlecode.gwtgl.binding.WebGLTexture;

import geometry.common.MessageOutput;
import geometry.math.Vector4d;


public class BufferColor implements DrawBuffer{
	 
	private int width, height;
	private WebGLTexture texColor;
	
	
	public BufferColor(WebGLRenderingContext gl, int width, int height){
		
		this.width = width;
		this.height = height;
		
		create(gl);
	}
	
	@Override
	public int getHeight(){ return height; }

	@Override
	public int getWidth(){ return width; }
	
	@Override
	public void resize(WebGLRenderingContext gl, int width, int height){

		delete(gl);
		
		this.width = width;
		this.height = height;
		
		create(gl);
	}
	
	private void create(WebGLRenderingContext gl){

		//generate textures
		texColor = gl.createTexture();
		
		gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, texColor);
		gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_MIN_FILTER, WebGLRenderingContext.LINEAR);
		gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_MAG_FILTER, WebGLRenderingContext.LINEAR);
		gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_WRAP_S, WebGLRenderingContext.CLAMP_TO_EDGE);
		gl.texParameteri(WebGLRenderingContext.TEXTURE_2D, WebGLRenderingContext.TEXTURE_WRAP_T, WebGLRenderingContext.CLAMP_TO_EDGE);
		gl.texImage2D(WebGLRenderingContext.TEXTURE_2D, 0, WebGLRenderingContext.RGBA, width, height, 0, WebGLRenderingContext.RGBA, WebGLRenderingContext.UNSIGNED_BYTE, null);
		
		gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, null);
		
		//check for errors
		switch(gl.getError()){
		case WebGLRenderingContext.INVALID_ENUM: throw new Error("(BufferColor) GL error GL_INVALID_ENUM");
		case WebGLRenderingContext.INVALID_VALUE: throw new Error("(BufferColor) GL error GL_INVALID_VALUE");
		case WebGLRenderingContext.INVALID_OPERATION: throw new Error("(BufferColor) GL error GL_INVALID_OPERATION");
		case WebGLRenderingContext.INVALID_FRAMEBUFFER_OPERATION: throw new Error("(BufferColor) GL error GL_INVALID_FRAMEBUFFER_OPERATION");
		case WebGLRenderingContext.OUT_OF_MEMORY: throw new Error("(BufferColor) GL error GL_OUT_OF_MEMORY");
		}
		
		MessageOutput.printDebug("Created draw buffer at " + width + "x" + height);
	}
	
	@Override
	public void delete(WebGLRenderingContext gl){
		
		gl.deleteTexture(texColor);
		
		MessageOutput.printDebug("Deleted draw buffer");
	}

	@Override
	public void clearDepth(WebGLRenderingContext gl){ }
	
	@Override
	public void clearColor(WebGLRenderingContext gl, Vector4d color){
		
		gl.colorMask(true, true, true, true);
		
		gl.clearColor(color.getX(), color.getY(), color.getZ(), color.getW());
		gl.clear(WebGLRenderingContext.COLOR_BUFFER_BIT);
	}

	@Override
	public void clearDepthAndColor(WebGLRenderingContext gl, Vector4d color){
		clearColor(gl, color);
	}
	
	@Override
	public void bindDrawTo(WebGLRenderingContext gl){
		
		//ensure render buffer is not attached
		gl.framebufferRenderbuffer(WebGLRenderingContext.FRAMEBUFFER, 
			WebGLRenderingContext.DEPTH_ATTACHMENT, WebGLRenderingContext.RENDERBUFFER, null);
		
		//attach texture
		gl.framebufferTexture2D(WebGLRenderingContext.FRAMEBUFFER, 
			WebGLRenderingContext.COLOR_ATTACHMENT0, WebGLRenderingContext.TEXTURE_2D, texColor, 0);
		
		//specify drawbuffers and readbuffers (unnecessary on GLES2)
		//gl.DrawBuffer(WebGLRenderingContext.COLOR_ATTACHMENT0); 
		//gl.ReadBuffer(WebGLRenderingContext.NONE);
		
		//check if it worked
		int status = gl.checkFramebufferStatus(WebGLRenderingContext.FRAMEBUFFER);
		if(status != WebGLRenderingContext.FRAMEBUFFER_COMPLETE){
			switch(status){
			case WebGLRenderingContext.FRAMEBUFFER_INCOMPLETE_ATTACHMENT: 
				throw new Error("(BufferColor) FBO status is GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT."); 
			case WebGLRenderingContext.FRAMEBUFFER_INCOMPLETE_DIMENSIONS: 
				throw new Error("(BufferColor) FBO status is GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS.");
			case WebGLRenderingContext.FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT: 
				throw new Error("(BufferColor) FBO status is GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT.");
			case WebGLRenderingContext.FRAMEBUFFER_UNSUPPORTED: 
				throw new Error("(BufferColor) FBO status is GL_FRAMEBUFFER_UNSUPPORTED.");
			default: 
				throw new Error("(BufferColor) FBO status is not GL_FRAMEBUFFER_COMPLETE.");
			} 
		}
			
		//set appropriate viewport
		gl.viewport(0, 0, width, height);
	}
	
	@Override
	public void unbindDrawTo(WebGLRenderingContext gl){

		//detach texture
		gl.framebufferTexture2D(WebGLRenderingContext.FRAMEBUFFER, 
			WebGLRenderingContext.COLOR_ATTACHMENT0, WebGLRenderingContext.TEXTURE_2D, null, 0);
	}
	
	public void bindReadFrom(WebGLRenderingContext gl){
		
		//attach texture
		gl.framebufferTexture2D(WebGLRenderingContext.FRAMEBUFFER, 
			WebGLRenderingContext.COLOR_ATTACHMENT0, WebGLRenderingContext.TEXTURE_2D, texColor, 0);
		
		//specify drawbuffers and readbuffers 
		//TODO: not necessary on GLES2?
		//gl.DrawBuffer(WebGLRenderingContext.NONE);
		//gl.ReadBuffer(WebGLRenderingContext.COLOR_ATTACHMENT0); 
		
		//check if it worked
		int status = gl.checkFramebufferStatus(WebGLRenderingContext.FRAMEBUFFER);
		if(status != WebGLRenderingContext.FRAMEBUFFER_COMPLETE){ 
			throw new Error("FBO status is not GL_FRAMEBUFFER_COMPLETE."); }
		
		//set appropriate viewport
		gl.viewport(0, 0, width, height);
	}
	
	public void unbindReadFrom(WebGLRenderingContext gl){
		
		//detach texture
		gl.framebufferTexture2D(WebGLRenderingContext.FRAMEBUFFER, 
			WebGLRenderingContext.COLOR_ATTACHMENT0, WebGLRenderingContext.TEXTURE_2D, null, 0);
	}
	
	@Override
	public void bindAsTexture(WebGLRenderingContext gl, int texUnit){
		
		gl.activeTexture(WebGLRenderingContext.TEXTURE0+texUnit);
		gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, texColor);
	}
	
	@Override
	public void unbindAsTexture(WebGLRenderingContext gl, int texUnit){

		gl.activeTexture(WebGLRenderingContext.TEXTURE0+texUnit);
		gl.bindTexture(WebGLRenderingContext.TEXTURE_2D, null);
	}
}