package engine.toolsAWT;

public class BufferTexture{

	public BufferTexture(int bufferPixelCapacity){ }
	

	/**
	 * Returns the R component of the pixel with offset (x,y) of the image
	 * currently stored in this BufferTexture.
	 */
	public int getR(int x, int y){ return 0; }
	
	/**
	 * Returns the G component of the pixel with offset (x,y) of the image
	 * currently stored in this BufferTexture.
	 */
	public int getG(int x, int y){ return 0; }
	
	/**
	 * Returns the B component of the pixel with offset (x,y) of the image
	 * currently stored in this BufferTexture.
	 */
	public int getB(int x, int y){ return 0; }
	
	/**
	 * Returns the A component of the pixel with offset (x,y) of the image
	 * currently stored in this BufferTexture.
	 */
	public int getA(int x, int y){ return 0; }
}
